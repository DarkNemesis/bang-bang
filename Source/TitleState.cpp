#include "TitleState.h"


TitleState::TitleState(StateStack& stack, Context context) : State(stack, context)
{
	context.mTextures->load(Textures::TitleScreen, "Images/TitleScreen.png");
	context.mTextures -> get(Textures::TitleScreen).setSmooth(true);
	mSprite.setTexture(context.mTextures -> get(Textures::TitleScreen));
	
	RenderWindow& window = *getContext().mWindow;
	FloatRect bounds = mSprite.getLocalBounds();
	mSprite.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
	mSprite.setPosition(window.getSize().x / 2.f, window.getSize().y / 2.f);

	mRect.setFillColor(Color(0,0,0,0));
	mRect.setSize(Vector2f(window.getSize()));
	mFade = 255;
	mFadeValue = -1;
}


TitleState::~TitleState(void)
{}

void TitleState::draw()
{
	RenderWindow& window = *getContext().mWindow;
	window.clear(Color(190,190,190,0));
	window.draw(mSprite);
	window.draw(mRect);
}

bool TitleState::processEvent(Event& event)
{
	if (event.type == Event::KeyPressed || event.type == Event::MouseButtonPressed)
	{
		requestStackPop();
		requestStackPush(States::Load);
	}
	return true;
}

bool TitleState::update(Time dt)
{
	mRect.setFillColor(Color(0,0,0,mFade));
	mFade += mFadeValue;
	if (mFade <= 50)
		mFadeValue = 2;
	if (mFadeValue > 0 && mFade >= 255)
	{
		requestStackPop();
		requestStackPush(States::Load);
	}
	return true;
}