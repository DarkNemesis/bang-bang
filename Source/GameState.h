#pragma once
#include <SFML/Graphics.hpp>
#include "World.h"
#include "Player.h"
#include "State.h"
using namespace sf;
class GameState : public State
{
	World mWorld;
	PlayerInput mPlayerInput;
public:
	GameState(StateStack& stack, Context context);
	~GameState(void);
	virtual bool update(Time dt);
	virtual bool processEvent(Event& event);
	virtual void draw();
};

