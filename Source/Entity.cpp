#include "Entity.h"
#define sign(a) (a / abs(a))
#define sign_inv(a) -1 * sign(a)

Entity::Entity()
{
	sDead = false;
}

void Entity::handleCollision(SceneNode* collider, bitset<8> polyRect)
{
}

void Entity::adaptPosition()
{
	if (getPosition().x >= mWorldSize.x - 20)
		setPosition(-1 * getSize().x + 20, getPosition().y);

	else if (getPosition().x + getSize().x <= 20)
		setPosition(mWorldSize.x - 20, getPosition().y);

	if (getPosition().y + getSize().y >= mWorldSize.y)
	{
		setPosition(getPosition().x, mWorldSize.y - getSize().y);
		mAcceleration.y += -Physics::cGravity;
		if (mVelocity.y > 0)
			mVelocity.y = 0;
	}
}

void Entity::updateCurrent(Time dt)
{
	applyPhysics(dt);
	move(mVelocity.x * dt.asSeconds(), mVelocity.y * dt.asSeconds());

	adaptPosition();
}

bool Entity::isMarkedForRemoval()
{
	return sDead;
}
bool Entity::isDestroyed()
{
	return sDead;
}
void Entity::destroy()
{
	sDead = true;
}