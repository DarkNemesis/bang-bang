#pragma once
#include "Entity.h"
#include <bitset>
class Platform;

class Block
{
protected:
	Vector2i	mPosition;
	int			mId;
	int			mPowerId;
	int			mBlockNumber;
	Vertex*		mVertexPointer;
	bool		isBouncing;

	friend class Platform;
	static Platform* mPlatformPointer;
public:
	Block(int blockID, int x, int y, int blockNo);
	~Block();	

	static Block*	createNewBlock(int blockID, int x, int y, int blockNo);

	void	setID(int id);
	int		getID();

	int			getBlockNumber();
	Vector2i	getPosition();
	
	void			issuePowerUpCommand(Vertex *quad);
	bool			issueGreenKey();

	virtual void	steppedOn(Entity *entity);
	virtual void	bouncedOff(Entity *entity);
	virtual void	collidedWith(Entity *entity, bitset<8> polyRect);
	
	void			updateTexCords();
};

class Spawner : public Block
{	
public:
	Spawner(int blockID, int x, int y, int blockNo);

	array<Vector2f, 2>	getSpawnInfo();

	void	steppedOn(Entity *entity);
	void	bouncedOff(Entity *entity);
	void	collidedWith(Entity *entity, bitset<8> polyRect);

	void	activateKey();
	int		getStatus();
};

class Portal : public Block
{
public:
	Portal(int blockID, int x, int y, int blockNo);
		
	void	steppedOn(Entity *entity);
	void	bouncedOff(Entity *entity);
	void	collidedWith(Entity *entity, bitset<8> polyRect);
};

class PowerBlock : public Block
{
public:
	PowerBlock(int blockID, int x, int y, int blockNo);
	void	bouncedOff(Entity *entity);
};





