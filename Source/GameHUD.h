#pragma once
#include "Scenenode.h"
#include "Player.h"

extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

class GameHUD :
	public SceneNode
{
	Sprite		mSprite;
	VertexArray	mPlayerHUD;
	lua_State*	L;
	Player*		mPlayer;
	Texture		mPlayerHUDTexture;
public:
	GameHUD();
	~GameHUD(void);

	virtual void			drawCurrent(RenderTarget& target, RenderStates states) const;
	virtual unsigned int	getCategory() const;
	virtual void			getCollisionRect(vector<FloatRect>& rectangles) const;
	void					updateCurrent(Time dt);

	FloatRect	getWorldSize();	
	void		setPlayerPtr(Player* player);
};

