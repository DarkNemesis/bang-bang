#include "StateStack.h"


StateStack::StateStack(State::Context context) : mContext(context)
{	
}


StateStack::~StateStack(void)
{
}

void StateStack::update(Time dt)
{
	for(auto itr = mStack.rbegin(); itr != mStack.rend(); ++itr)
		if (!(*itr) -> update(dt))
			break;
	applyPendingChanges();
}

void StateStack::draw()
{
	for(unique_ptr<State>& state : mStack)
		state -> draw();
}

void StateStack::processEvent(Event& event)
{
	for(auto itr = mStack.rbegin(); itr != mStack.rend(); ++itr)
		if (!(*itr) -> processEvent(event))
			break;
	applyPendingChanges();
}

void StateStack::pushState(States::States stateID)
{
	mPendingList.push_back(PendingChange(Push, stateID));
}

void StateStack::popState()
{
	mPendingList.push_back(PendingChange(Pop));
}

void StateStack::clearState()
{
	mPendingList.push_back(PendingChange(Clear));
}

bool StateStack::isEmpty() const
{
	return mStack.empty();
}

unique_ptr<State> StateStack::createState(States::States stateID)
{
	auto found = mFactories.find(stateID);
	return found->second();	
}

void StateStack::applyPendingChanges()
{
	for(PendingChange change : mPendingList)
	{
		switch(change.mAction)
		{
		case Push	:	mStack.push_back(createState(change.mStateID));
						break;
		case Pop	:	mStack.pop_back();
						break;
		case Clear	:	mStack.clear();
						break;
		}
	}
	mPendingList.clear();
}