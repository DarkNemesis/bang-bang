#include "User.h"


User::User(void) : mRegister(false)
{
}


User::~User(void)
{
}

void User::setName(string name)
{
	mUserName = name;
}

void User::setBaseValue(unsigned char life, Int64 score, unsigned char world, unsigned char level)
{
	mLife = life;
	mScore = score;
	mWorld = world;
	mLevel = level;
}

void User::fileInput(string filename)
{
	fstream fin(filename);
	fin >> mLife;
	fin >> mScore;
	fin >> mWorld;
	fin >> mLevel;
}