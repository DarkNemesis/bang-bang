#include "PowerUp.h"
#include "Tank.h"
#include "Opponent.h"
#include "Player.h"
#include "Platform.h"

PowerUp::PowerUp()
{
	mCategory = Category::PowerUp;
	mMarkedForRemoval = false;
}

PowerUp::PowerUp(unsigned int id)
{
	mDoesItCollide = true;

	mMarkedForRemoval = false;
	mID = id;
	mSprite.setTexture(mTextures->get(Textures::PowerUps));
	mCategory = Category::PowerUp;

	//Open the lua script file
	string filename = "Scripts/PowerUps.lua";
	lua_State* L;
	L = luaL_newstate();
	luaL_openlibs(L);
	luaL_dofile(L, filename.c_str());

		
	//Get the table for this particular Texture
	lua_pushinteger(L, mID + 1);
	lua_gettable(L, -2);
	
	lua_getfield(L, -1, "left");
	mTextureRect.left = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "top");
	mTextureRect.top = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "width");
	mTextureRect.width = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, -1, "height");
	mTextureRect.height = lua_tointeger(L, -1);
	lua_pop(L, 3);

	mSprite.setTextureRect(mTextureRect);
}

PowerUp::PowerUp(unsigned int id, Vector2f position) : PowerUp(id)
{
	setPosition(position);
}

void PowerUp::setID(unsigned int id)
{
	PowerUp::PowerUp(id);
}

Vector2f PowerUp::getSize()
{
	return Vector2f(mTextureRect.width, mTextureRect.height);
}

void PowerUp::getCollisionRect(vector<FloatRect>& rectangles) const
{
	rectangles.clear();
	FloatRect bounds = getWorldTransform().transformRect(mSprite.getGlobalBounds());
	FloatRect rect = FloatRect(0, 0, mTextureRect.width, mTextureRect.height);
	rectangles.push_back(rectShift(rect, bounds));
}

void PowerUp::drawCurrent(RenderTarget& target, RenderStates states) const
{
	target.draw(mSprite, states);
}

void PowerUp::updateCurrent(Time dt)
{
	Entity::updateCurrent(dt);
}

void PowerUp::handleCollision(SceneNode* collider, int polyRect)
{
	Command power;
	
	switch (collider->getCategory())
	{

	case Category::TankA:	power.mCategory = Category::Player;
							mSFX.playSFX(SFX::PowerObtained);
							switch (mID)
							{
							case RedKey:	power.mAction = [](SceneNode& node, Time dt) {Player& tank = static_cast<Player&>(node); tank.addKey(LevelKeys::Red); };
											break;
							case YellowKey: power.mAction = [](SceneNode& node, Time dt) {Player& tank = static_cast<Player&>(node); tank.addKey(LevelKeys::Yellow); };
											break;
							case BlueKey:	power.mAction = [](SceneNode& node, Time dt) {Player& tank = static_cast<Player&>(node); tank.addKey(LevelKeys::Blue); };
											break;
							case GreenKey:	power.mAction = [](SceneNode& node, Time dt) {Player& tank = static_cast<Player&>(node); tank.addKey(LevelKeys::Green); };
											break;
							default:		return;
							}
							break;

	case Category::Tile_Normal:
	case Category::Tile_Bouncing:	if (mAcceleration.y) mAcceleration.y -= Physics::cGravity;
									mVelocity.y = 0;
									return;

	default:				return;

	}
	mCommandQueue.push(power);
	destroy();
}

bool PowerUp::isMarkedForRemoval()
{
	return mMarkedForRemoval;
}

void PowerUp::destroy()
{
	mMarkedForRemoval = true;
}

void Objects::createPowerUp(unsigned int ID, Vector2f position)
{
	unique_ptr<PowerUp> power(new PowerUp(ID, position));
	attachChild(std::move(power));
	mSFX.playSFX(SFX::NewKey);
}

void Objects::useKey(unsigned int keyId, Vector2f lockPosition)
{
	if (keyId == LevelKeys::Red)
	{
		Command cmd;
		cmd.mCategory = Category::Opponent;
		cmd.mAction = [](SceneNode& node, Time dt)
		{Opponent& opp = static_cast<Opponent&>(node); opp.requestForKeyGeneration(); };
		mCommandQueue.push(cmd);
	}
	else if (keyId == LevelKeys::Yellow)
	{
		Command cmd;
		cmd.mCategory = Category::Block;
		cmd.mAction = [](SceneNode& node, Time dt)
		{Platform& plat = static_cast<Platform&>(node); plat.generateGreenKey(); };
		mCommandQueue.push(cmd);
	}
	/*else if (keyId == LevelKeys::Green)
	{
		Command cmd;
		cmd.mCategory = Category::Opponent;
		cmd.mAction = [lockPosition](SceneNode& node, Time dt)
		{Opponent& Node = static_cast<Opponent&>(node); Node.removeSpawner(lockPosition); };
		mCommandQueue.push(cmd);
	}*/
}

Objects::Objects()
{
	mCategory = Category::Objects;
}
