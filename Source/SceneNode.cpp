#include "SceneNode.h"
Vector2u SceneNode::mWindowSize;
Vector2f SceneNode::mWorldSize;
TextureHolder* SceneNode::mTextures;
SoundPlayer SceneNode::mSFX;
CommandQueue SceneNode::mCommandQueue;

void SceneNode::attachChild(SN_PTR child)
{
	child -> mParent = this;
	mChildren.push_back(std::move(child));
}

unique_ptr<SceneNode> SceneNode::detachChild(SceneNode& node)
{
	auto found = std::find_if(mChildren.begin(), mChildren.end(), [&] (SN_PTR& p) {return p.get() == &node;});
	SN_PTR result = std::move(*found);
	result -> mParent = nullptr;
	mChildren.erase(found);
	return result;
}

void SceneNode::updateCurrent(Time dt)
{}

void SceneNode::update(Time dt)
{
	updateCurrent(dt);
	updateChildren(dt);
}

void SceneNode::updateChildren(Time dt)
{
	for(SN_PTR& child : mChildren)
		child -> update(dt);
}

void SceneNode::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	drawCurrent(target, states);
	drawChildren(target, states);
}

void SceneNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{}

void SceneNode::drawChildren(sf::RenderTarget& target, sf::RenderStates states) const
{
	for(const SN_PTR& child : mChildren)
		child -> draw(target, states);
}

void SceneNode::onCommand(Command& command, Time dt)
{
	if (command.mCategory & getCategory())
		command.mAction(*this, dt);

	for(SN_PTR& child : mChildren)
		child -> onCommand(command, dt);
}

unsigned int SceneNode::getCategory() const
{
	return mCategory;
}

unsigned int SceneNode::getCollidingCategory() const
{
	return mCollidingCategory;
}

void SceneNode::getCollisionRect(vector<FloatRect>& rectangles) const
{
	rectangles.clear();
	rectangles.push_back(FloatRect());	
}

void SceneNode::setCategory(unsigned int category)
{
	mCategory = category;
}

void SceneNode::setCollidingCategory(unsigned int category)
{
	mCollidingCategory = category;
}

void SceneNode::checkDoesNodeCollide(vector<SceneNode*>& collidingNodes)
{
	if (mDoesItCollide)
		collidingNodes.push_back(this);
	for (unique_ptr<SceneNode>& child : mChildren)
		child->checkDoesNodeCollide(collidingNodes);
}
void SceneNode::checkSceneCollision(SceneNode& sceneGraph, set<Pair>& collisionPairs)
{
	checkNodeCollision(sceneGraph, collisionPairs);
	for (unique_ptr<SceneNode>& child : sceneGraph.mChildren)
		checkSceneCollision(*child, collisionPairs);
}

void SceneNode::checkNodeCollision(SceneNode& node, set<Pair>& collisionPairs)
{
	if (this != &node && collision(*this, node))
		collisionPairs.insert(minmax(this, &node));
	for (unique_ptr<SceneNode>& child : mChildren)
		child -> checkNodeCollision(node, collisionPairs);
}

Transform SceneNode::getWorldTransform() const
{
	Transform transform = Transform::Identity;
	for (const SceneNode* node = this; node != nullptr; node = node->mParent)
		transform = node -> getTransform() * transform;
	return transform;
}

bool SceneNode::isMarkedForRemoval()
{
	return false;
}

bool SceneNode::isDestroyed()
{
	return false;
}

void SceneNode::removeNodes()
{
	auto wreckfield = remove_if(mChildren.begin(), mChildren.end(), mem_fn(&SceneNode::isMarkedForRemoval));
	mChildren.erase(wreckfield, mChildren.end());

	std::for_each(mChildren.begin(), mChildren.end(), mem_fn(&SceneNode::removeNodes));
}

void SceneNode::setSize(Vector2f size)
{
	mSize = size;
}

Vector2f SceneNode::getSize()
{
	return mSize;
}

SpriteNode::SpriteNode()
{
	Texture& tex = mTextures->get(Textures::Platform);
	mSprite.setTexture(tex);
	setScale(mWindowSize.x * 1.0 / tex.getSize().x, mWindowSize.y * 1.0 / tex.getSize().y);
}

void SpriteNode::drawCurrent(RenderTarget& target, RenderStates states) const
{
	target.draw(mSprite, states);
}






