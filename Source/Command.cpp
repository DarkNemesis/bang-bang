#include "Command.h"
Command::~Command(void)
{
}

Command::Command(void)
{
	mCategory = Category::None;
}

void CommandQueue::push(Command& command)
{
	mQueue.push(command);
}

bool CommandQueue::isEmpty()
{
	return mQueue.empty();
}

Command CommandQueue::pop()
{
	Command command = mQueue.front();
	mQueue.pop();
	return command;
}