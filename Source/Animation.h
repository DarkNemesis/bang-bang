#pragma once
#include <SFML/Graphics.hpp>
using namespace sf;
class Animation
	: public Transformable, public Drawable
{
	IntRect mFrameRect;
	size_t mFrames;
	size_t mCurrentFrame;
	Time mDuration;
	Time mElapsedTime;
    bool mRepeat;
	void draw(RenderTarget& target, RenderStates states) const;

public:

	Sprite mSprite;
					Animation();
					Animation(const sf::Texture& texture, IntRect frameRect);
	
	void 			setTexture(const Texture& texture);
	const Texture* 	getTexture() const;

	void 			setFrameRect(IntRect mFrameRect);
	IntRect			getFrameRect() const;

	void 			setNumFrames(size_t numFrames);
	size_t 			getNumFrames() const;

	void 			setDuration(Time duration);
	Time 			getDuration() const;

	void 			setRepeating(bool flag);
	bool 			isRepeating() const;

	void 			restart();
	bool 			isFinished() const;

	FloatRect		getLocalBounds() const; 
	FloatRect 		getGlobalBounds() const;

	void 			update(Time dt);
};

