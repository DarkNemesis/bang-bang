#pragma once
#include "SceneNode.h"
#include "Block.h"
#include <random>

typedef uniform_real_distribution<double> uniReal;
class Opponent : public SceneNode
{
	int		mSpawnQueue;

	float	mSpawnTimer;
	float	mSpawnDelay;

	uniReal		mRandomDistributor;
	mt19937_64	mRandomGenerator;
		
	vector<Spawner*> mSpawners;
	int		mKeyRequestStack;
	float	mKeyProb;
public:
	Opponent();
	void	requestForKeyGeneration();
	void	updateCurrent(Time dt);
	void	addSpawner(Spawner* spawner);
	void	refreshSpawnerList();
	void	warp(SceneNode* node);
	void	spawn(int n = 1);
	int		getSpawnerCount();
};