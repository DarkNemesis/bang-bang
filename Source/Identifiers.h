#pragma once
#include "ResourceHolder.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

namespace Category
{
	enum Category
	{
		None = 0,
		Scene = 1 << 0,
		Block = 1 << 1,
		TankA = 1 << 2,
		Animal = 1 << 3,
		Opponent = 1 << 4,
		PowerUp = 1 << 5,
		Objects = 1 << 6,
		Player = 1 << 7
	};	
	enum ColliderCategory
	{
		//Types of Tile
		Tile_Normal, Tile_Bouncing, Tile_Portal, 
		//Spawners
		Spawner_Red, Spawner_Yellow, Spawner_Green, Spawner_Blue,
		//Entities
		Entity_Animal
	};
}



namespace States
{
	enum States
	{
		None,
		Title,
		Menu,
		Load,
		Game,
		Pause
	};
}

namespace Textures
{
	enum Textures
	{
		Platform,
		Tile,
		Tank,
		TankExplosion,
		TitleScreen,
		MenuScreen,
		HUDBanner,
		ButtonNormal,
		ButtonSelected,
		ButtonActive,
		Wolf,
		LoadingScreen,
		PowerUps,
		Vanish
	};
}

namespace MusicTheme
{
	enum MusicTheme
	{
		MenuTheme,
		GameTheme
	};
}

namespace Fonts
{
	enum Fonts
	{
		MenuTitle,
		MenuButton
	};
}

namespace Modes
{
	enum Modes
	{
		Walk,
		Repair,
		Dead,
		Charge,
		Vanish
	};
}

namespace SFX
{
	enum SFX
	{
		PlayerDeath,
		RoboRepair,
		RoboEntry,
		RoboDelete,
		NewKey,
		PowerObtained,
		PortalExplosion,
		RoboVanish
	};
}

typedef ResourceHolder<sf::Font, Fonts::Fonts> FontHolder;
typedef ResourceHolder<sf::Texture, Textures::Textures> TextureHolder;
typedef ResourceHolder<sf::SoundBuffer, SFX::SFX> SFXHolder;
