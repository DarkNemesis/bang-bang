#include "State.h"
#include "StateStack.h"

State::Context::Context(RenderWindow& window, TextureHolder& textures, PlayerInput& playerInput, MusicPlayer& music, FontHolder& fonts, SoundPlayer& sfx, User& user)
	: mWindow(&window), mTextures(&textures), mPlayer(&playerInput), mMusic(&music), mFonts(&fonts), mSFX(&sfx), mUser(&user)
{
}

State::State(StateStack& stack, Context context) : mContext(context), mStack(&stack)
{
}

State::~State(void)
{
}

void State::requestStackPush(States::States stateID)
{
	mStack -> pushState(stateID);
}

void State::requestStackPop()
{
	mStack -> popState();
}

void State::requestStackCLear()
{
	mStack -> clearState();
}

State::Context State::getContext() const
{
	return mContext;
}