#include "ParallelTask.h"
#include <direct.h>
#include <fstream>

ParallelTask::ParallelTask(State::Context &context) : mFinished(false), mContext(context)
{}

ParallelTask::~ParallelTask(void)
{}

bool ParallelTask::isFinished()
{
	return mFinished;
}

void runTask(State::Context &context, bool *status)
{
	context.mTextures -> load(Textures::MenuScreen, "Images/MenuScreen.png");
	context.mTextures -> get(Textures::MenuScreen).setSmooth(true);

	context.mTextures -> load(Textures::ButtonNormal, "Images/NormalButton.png");
	context.mTextures -> load(Textures::ButtonActive, "Images/ActiveButton.png");
	context.mTextures -> load(Textures::ButtonSelected, "Images/SelectedButton.png");

	context.mFonts -> load(Fonts::MenuTitle, "Fonts/MenuTitle.ttf");
	context.mFonts -> load(Fonts::MenuButton, "Fonts/MenuButton.ttf");
		
	if(_chdir("User"))
		_mkdir("User");
	else
	{
		fstream readPlayer("UserData.bin", ios::in);
		string filename;
		if (!readPlayer.fail())
		{
			vector<string> userNames;
			do
			{	
				string name;
				std::getline(readPlayer, name);
				userNames.push_back(name);
			} while(!readPlayer.eof());
			readPlayer.close();	

			for (auto playerPtr = userNames.begin(); playerPtr != userNames.end();)			
			{
				string readingName = *playerPtr;
				readPlayer.open(readingName + ".bin", ios::in);
				if (readPlayer.fail())
				{
					playerPtr++;					
					continue;
				}
				else
				{
					context.mUser -> fileInput(readingName + ".bin");
					context.mUser -> mRegister = true;
					context.mUser -> mUserName = readingName;
					break;
				}
			}
		}
		_chdir("..");
	}
	*status = true;	
}

void ParallelTask::execute()
{
	mThread = thread(&runTask, mContext, &mFinished);	
}

void ParallelTask::joinRequest()
{
	mThread.join();
}