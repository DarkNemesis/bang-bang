#pragma once
#include <SFML\Graphics.hpp>
#include <string>
extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}
using namespace std;
static int luaSetPrimitiveType(lua_State *L)
{
	using namespace sf;
	VertexArray* vertexArray = (sf::VertexArray*)lua_touserdata(L, 1);
	string primitiveType(lua_tostring(L, -1));

	if (primitiveType == "Quad")
		vertexArray->setPrimitiveType(Quads);
	else if (primitiveType == "Line")
		vertexArray->setPrimitiveType(Lines);
	else if (primitiveType == "LineStrip")
		vertexArray->setPrimitiveType(LinesStrip);
	else if (primitiveType == "Triangle")
		vertexArray->setPrimitiveType(Triangles);
	else if (primitiveType == "TriangleFan")
		vertexArray->setPrimitiveType(TrianglesFan);
	else if (primitiveType == "TriangleStrip")
		vertexArray->setPrimitiveType(TrianglesStrip);
	else if (primitiveType == "Point")
		vertexArray->setPrimitiveType(Points);
	return 1;
}

static int luaResize(lua_State *L)
{
	using namespace sf;
	VertexArray* vertexArray = (VertexArray*)lua_touserdata(L, 1);
	int resizeFactor = lua_tointeger(L, 2);
	vertexArray->resize(resizeFactor);
	return 0;
}

static int luaSetTexCords(lua_State *L)
{
	using namespace sf;
	VertexArray* vertexArray = (VertexArray*)lua_touserdata(L, 1);
	int index = lua_tointeger(L, 2);
	index--;
		
	lua_getfield(L, 3, "left");
	int left = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 3, "top");
	int top = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 3, "width");
	int width = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 3, "height");
	int height = lua_tointeger(L, -1);
	lua_pop(L, 1);
	
	Vertex* quad = &((*vertexArray)[index * 4]);
		
	quad[0].texCoords = Vector2f(left, top);
	quad[1].texCoords = Vector2f(left + width, top);
	quad[2].texCoords = Vector2f(left + width, top + height);
	quad[3].texCoords = Vector2f(left, top + height);
	
	return 1;
}

static int luaSetColor(lua_State *L)
{
	using namespace sf;
	VertexArray* vertexArray = (VertexArray*)lua_touserdata(L, 1);
	int index = lua_tointeger(L, 2);
	int length = lua_tointeger(L, 3);
	Color point;

	lua_pushinteger(L, 1);
	lua_gettable(L, -2);
	int r = lua_tointeger(L, -1);	lua_pop(L, 1);

	lua_pushinteger(L, 2);
	lua_gettable(L, -2);
	int g = lua_tointeger(L, -1);	lua_pop(L, 1);

	lua_pushinteger(L, 3);
	lua_gettable(L, -2);
	int b = lua_tointeger(L, -1);	lua_pop(L, 1);

	lua_pop(L, 1);
	point = Color(r, g, b);

	for (int i = index; i < length + index; i++)
	{
		Vertex* quad = &((*vertexArray)[i]);
		quad[0].color = point;
	}
	return 1;
}

static int luaSetPosition(lua_State *L)
{
	using namespace sf;
	VertexArray* vertexArray = (VertexArray*)lua_touserdata(L, 1);
	int index = lua_tointeger(L, 2);
	index--;

	lua_getfield(L, 3, "left");
	int left = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 3, "top");
	int top = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 3, "width");
	int width = lua_tointeger(L, -1);
	lua_pop(L, 1);

	lua_getfield(L, 3, "height");
	int height = lua_tointeger(L, -1);
	lua_pop(L, 1);

	Vertex* quad = &((*vertexArray)[index * 4]);

	quad[0].position = Vector2f(left, top);
	quad[1].position = Vector2f(left + width, top);
	quad[2].position = Vector2f(left + width, top + height);
	quad[3].position = Vector2f(left, top + height);

	return 1;
}

static const luaL_Reg vertexLib[] =
{
	{ "resize", luaResize },
	{ "setType", luaSetPrimitiveType },
	{ "position", luaSetPosition },
	{ "texture", luaSetTexCords },
	{ "recolor", luaSetColor },
	{ NULL, NULL }
};

extern void luaVertexLib(lua_State *L)
{
	luaL_newlib(L, vertexLib);
	lua_setglobal(L, "vertex");
}