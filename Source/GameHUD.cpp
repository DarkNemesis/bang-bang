#include "GameHUD.h"
#include "Lua_VertexArray.h"

GameHUD::GameHUD()
{
	Texture& texture(mTextures->get(Textures::HUDBanner));
	texture.setSmooth(true);

	mSprite.setTexture(texture);
	mSprite.setScale(mWindowSize.x / mSprite.getLocalBounds().width, mWindowSize.y * 0.2 / mSprite.getLocalBounds().height);
	mSprite.setPosition(0, 0.8 * mWindowSize.y);	

	//Load the new Lua State
	L = luaL_newstate();
	luaL_openlibs(L);

	//Register Vertex Library
	luaVertexLib(L);
	
	//Register Pokemon
	lua_pushlightuserdata(L, &mPlayerHUD);
	lua_setglobal(L, "PlayerHUD");

	//Load Script File
	luaL_dofile(L, "Scripts/HUDElements.lua");

	mPlayerHUD.setPrimitiveType(Quads);
	mPlayerHUDTexture.loadFromFile("Images/HUDElements.png");

	lua_getglobal(L, "init");

	lua_createtable(L, 0, 4);

	lua_pushstring(L, "left");
	lua_pushnumber(L, mSprite.getPosition().x);
	lua_settable(L, -3);

	lua_pushstring(L, "top");
	lua_pushnumber(L, mSprite.getPosition().y);
	lua_settable(L, -3);

	lua_pushstring(L, "width");
	lua_pushnumber(L, mSprite.getGlobalBounds().width);
	lua_settable(L, -3);

	lua_pushstring(L, "height");
	lua_pushnumber(L, mSprite.getGlobalBounds().height);
	lua_settable(L, -3);

	lua_call(L, 1, 0);
}

GameHUD::~GameHUD(void)
{
}

void GameHUD::drawCurrent(RenderTarget& target, RenderStates states) const
{
	target.draw(mSprite, states);

	RenderStates playerHUDStates = states;
	playerHUDStates.texture = &mPlayerHUDTexture;
	target.draw(mPlayerHUD, playerHUDStates);
}

void GameHUD::setPlayerPtr(Player* player)
{
	mPlayer = player;
}

unsigned int GameHUD::getCategory() const
{
	return Category::Scene;
}

void GameHUD::getCollisionRect(vector<FloatRect>& rectangles) const
{
	rectangles.clear();
	rectangles.push_back(getWorldTransform().transformRect(mSprite.getGlobalBounds()));	
}

void GameHUD::updateCurrent(Time dt)
{
	lua_getglobal(L, "update");
	lua_pushnumber(L, mPlayer->mPlayerLife);

	lua_createtable(L, 0, 4);
	lua_pushstring(L, "Red");
	lua_pushnumber(L, mPlayer->mKeyQuantity[0]);
	lua_settable(L, -3);
	lua_pushstring(L, "Yellow");
	lua_pushnumber(L, mPlayer->mKeyQuantity[1]);
	lua_settable(L, -3);
	lua_pushstring(L, "Green");
	lua_pushnumber(L, mPlayer->mKeyQuantity[2]);
	lua_settable(L, -3);
	lua_pushstring(L, "Blue");
	lua_pushnumber(L, mPlayer->mKeyQuantity[3]);
	lua_settable(L, -3);
	lua_call(L, 2, 0);
}

FloatRect GameHUD::getWorldSize()
{
	return mSprite.getGlobalBounds();
}
