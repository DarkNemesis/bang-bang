#pragma once
#include <SFML/Graphics.hpp>
#include <thread>
#include <memory>
#include "State.h"
#include "Identifiers.h"

using namespace sf;
class ParallelTask
{
	thread mThread;
	//Mutex mMutex;
	bool mFinished;	
public:
	State::Context &mContext;
	ParallelTask(State::Context &context);
	~ParallelTask(void);
	void joinRequest();
	void execute();
	bool isFinished();
};

