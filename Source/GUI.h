#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include <functional>
#include <array>
#include "ResourceHolder.h"
#include "Identifiers.h"
using namespace sf;
using namespace std;
namespace GUI
{
	//	--- COMPONENT DECLARATIONS ---	//

	class Component : public Drawable, public Transformable
	{
	protected :
		bool _isHighlighted;
		bool _isSelected;
		bool mFinish;
	public :
		Component();		
		
		virtual void highlight(bool highlight);
		virtual void select(bool select);
		virtual bool isSelected();
		virtual bool isFinished();

		virtual bool handleEvent(const Event& event);
		virtual FloatRect boundingBox() = 0;
		virtual void execute() = 0;
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
	};

	//	===###--- BUTTON DECLARATIONS ===###---	//

	/*	
		Defines class for a normal button. 
		Button has four states : normal, mouseover, selected and activated.
		Buttons by default just have a background color, but can have a text or a texture.		
	*/

	class Button : public Component
	{
	public :
		enum eButtonState	{Normal, Highlighted, Selected, Disabled, ButtonStates};
		enum eButtonType	{Textured, Labeled, TexLabeled, Colored, Default};
		enum eButtonShape	{Triangle, Rectangle, Circle};
		enum eColor			{Background, Border, Label, Colors};

	private :
		Text	mLabel;								//Text Label for the button.
		bool	_isBackgroundEnabled;				//Indicates whether the background color is enabled.
		void	switchState(eButtonState target);

		eButtonType			mButtonType;
		eButtonShape		mButtonShape;
		unique_ptr<Shape>	mButton;	
		function<void()>	mAction;

		array<Texture*, ButtonStates>				mTexture;
		array<array<Color, ButtonStates>, Colors>	mColors;		
	public :
		Button(eButtonType type = Default);

		/**
			Changes the shape of the button.
			The properties of previous shape are carried over.

			@param shape The new shape of the button
		*/
		void	setShape(eButtonShape shape);

		/**	
			Set the thickness of the shape's outline.
			Note that negative values are allowed (so that the outline
			expands towards the center of the shape), and using zero
			disables the outline. By default, the outline thickness is 0.
    
			@param thickness New outline thickness
		**/	
		void	setOutlineThickness(int thickness);

		/**
			Set the size of the button's shape.
			In case of rectangles, width and height need to be mentioned.
			In case of other polygons (including square), only the radius needs to be mentioned.

			@param x Size in x coordinates, Radius for other polygons
			@param y Size in y coordinates, default zero for other polygons
		**/
		void	setSize(int x, int y = 0);

		
		void	setColors(eColor target = Background, vector<Color> colors = { { Color::Black, Color::Green, Color::Red, Color(128, 128, 128) } });
		void	setTexture(vector<Texture*> textures);
		void	setLabel(string label, Font& font, unsigned int size);
		void	setAction(function<void()> execute);

		void	highlight(bool highlight);
		void	select(bool select);
		bool	isSelected();
		void	execute();
				
		
		FloatRect boundingBox();		
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	};
	//	--- TEXTBOX DECLARATIONS ---	//

	class TextBox : public Component
	{
		Text mText;		
		RectangleShape mShape;
		unsigned int mKeyLength;
		function<void()> mExecute;
		shared_ptr<string> mDestString;

	public :
		TextBox(Vector2f position, Vector2f size, shared_ptr<string> destString);
		bool handleEvent(const Event& event);
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		void execute();
		void select(bool select);

		FloatRect boundingBox();	

		void setSize(Vector2f size);
		void setPosition(Vector2f position);
		void setColor(Color color);
		void setBackColor(Color colorF, Color colorO);
		void setString(string label);
		void setFont(Font& font);
		void setKeyLength(unsigned int length);
		void setFunction(function<void()> funct);		
	};

	//	--- CONTAINER DECLARATIONS ---	//

	class Container : public Drawable, public Transformable
	{
		vector<Component*> mChildren;
		RenderWindow& mWindow;
	public :
		Container(RenderWindow& window);
		void handleEvent(const Event& event);
		void update();
		virtual void draw(RenderTarget& target, RenderStates states) const;
		void pack(Component* component);
	};
}