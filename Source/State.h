#pragma once
#include "Identifiers.h"
#include "ResourceHolder.h"
#include "MusicPlayer.h"
#include <SFML/Graphics.hpp>
#include <memory>
#include "User.h"

using namespace sf;
using namespace std;
class StateStack;
class PlayerInput;
class SoundPlayer;

class State
{
public: 
	struct Context
	{
		Context(RenderWindow& window, TextureHolder& textures, PlayerInput& playerInput, MusicPlayer& music, FontHolder& fonts, SoundPlayer& sfx, User& user);
		RenderWindow* mWindow;
		TextureHolder* mTextures;
		PlayerInput* mPlayer;
		MusicPlayer* mMusic;
		FontHolder* mFonts;
		SoundPlayer* mSFX;
		User* mUser;
	};

	State(StateStack& stack, Context context);
	virtual ~State(void);
	virtual void draw() = 0;
	virtual bool update(Time dt) = 0;
	virtual bool processEvent(Event& event) = 0;
		
protected:
	void requestStackPush(States::States stateID);
	void requestStackPop();
	void requestStackCLear();
	Context getContext() const;

private:
	Context mContext;
	StateStack* mStack;
};

