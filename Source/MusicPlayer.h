#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Identifiers.h"
#include <map>
using namespace std;
using namespace sf;
class MusicPlayer : private NonCopyable
{
	Music mMusic;
	map<MusicTheme::MusicTheme, string> mFilenames;
	float mVolume;
public:
	MusicPlayer(void);
	~MusicPlayer(void);
	void play(MusicTheme::MusicTheme musicID);

};

