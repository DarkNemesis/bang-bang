#pragma once
#include <SFML/Graphics.hpp>
#include "Animation.h"
#include "SceneNode.h"
using namespace sf;

template <typename spaceVar>
void centerOrigin(spaceVar &object)
{
	FloatRect bounds = object.getLocalBounds();
	object.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
}

void		centerOrigin(Animation& animation);
void		windowScale(Vector2u targetSize, Sprite& targetSprite);
FloatRect	rectShift(FloatRect target, FloatRect dest);
FloatRect	rectShift(IntRect target, FloatRect dest);
FloatRect	rectIntToFloat(IntRect target);

bool		collision(const SceneNode& lhs, const SceneNode& rhs);
bool		collision(FloatRect lhsRect, const SceneNode& rhs);
