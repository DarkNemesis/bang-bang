#include "GameState.h"

GameState::GameState(StateStack& stack, Context context) : mWorld(*context.mWindow), mPlayerInput(*context.mPlayer), State(stack, context)
{
	context.mMusic -> play(MusicTheme::GameTheme);
	mWorld.mDestroy = [this] () {requestStackPop();};
}


GameState::~GameState(void)
{
}


bool GameState::update(Time dt)
{
	mWorld.update(dt);

	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayerInput.handleRealtimeInput(commands);
	return true;
}

bool GameState::processEvent(Event& event)
{
	CommandQueue& commands = mWorld.getCommandQueue();
	mPlayerInput.handleEvent(event, commands);
	if (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
		requestStackPop();
	return true;
}

void GameState::draw()
{
	mWorld.draw();	
}