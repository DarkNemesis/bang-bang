#pragma once
#include "StateStack.h"
#include "Identifiers.h"
#include "Player.h"
#include "ResourceHolder.h"
#include "TitleState.h"
#include "GameState.h"
#include "MenuState.h"
#include "LoadingState.h"
#include "MusicPlayer.h"
#include "User.h"
#include "SoundPlayer.h"
#include <SFML/Graphics.hpp>

using namespace sf;
class Application
{
	RenderWindow mWindow;
	ResourceHolder<Texture, Textures::Textures> mTextures;
	PlayerInput mPlayerInput;
	StateStack	mStateStack;
	Time		mFrameTime;
	VideoMode	mMode;
	MusicPlayer mMusic;
	FontHolder	mFonts;
	User		mUser;
	SoundPlayer mSound;
	Image		mApplicationIcon;
public:
	Application(void);
	~Application(void);

	void update(Time dt);
	void processEvent();
	void render();
	void run();

	void registerStates();
};

