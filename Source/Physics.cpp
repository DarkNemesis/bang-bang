#include "Physics.h"
#include "Collision.h"
const double Physics::cGravity = 1500;

Physics::Physics() : mDrag(0, 0)
{
	mAcceleration = Vector2f(0, 0);
	mVelocity = Vector2f(0, 0);
}

void Physics::setVelocity(Vector2f velocity)
{
	mVelocity = velocity;
}

Vector2f Physics::getVelocity()
{
	return mVelocity;
}

void Physics::jump()
{
	if (mVelocity.y == 0 && mAcceleration.y == 0)
		mVelocity.y = -700;
}

void Physics::applyPhysics(Time dt)
{	
	if (isStandingOnSolidGround == false)
		mAcceleration.y = cGravity;
	else
		mAcceleration.y = 0;

	mAcceleration.x += computeDrag(mVelocity.x);
	double old = mVelocity.x;
	mVelocity = mVelocity + (mAcceleration * dt.asSeconds());
	if (old && (signbit(old) != signbit(mVelocity.x)))
	{
		mAcceleration.x = 0;
		mVelocity.x = 0;
	}
	isStandingOnSolidGround = false;
}

void Physics::setVelocity(float x, float y)
{
	mVelocity.x = x;
	mVelocity.y = y;
}

Vector2f Physics::getAcceleration()
{
	return mAcceleration;
}
void Physics::setAcceleration(Vector2f acceleration)
{
	mAcceleration = acceleration;
}
void Physics::setAcceleration(float x, float y)
{
	mAcceleration.x = x;
	mAcceleration.y = y;
}

void Physics::standOnSolidGround()
{
	isStandingOnSolidGround = true;
}

double Physics::computeDrag(double velocity)
{
	int sign = ((velocity >= 0) ? 1 : -1);
	return (-sign * ((mDrag.k1 * abs(velocity)) + (mDrag.k2 * pow(velocity, 2))));
}

Physics::~Physics()
{
}

bool SolidObject::smallerThan(SolidObject a)
{
	return true;
	//return (mValue < a.mValue);
}

SolidObject::SolidObject()
{	
	Collision::registerSolidObject(this);
}

SolidObject::~SolidObject()
{
	Collision::deleteSolidObject(this);
}

void SolidObject::update(FloatRect bounds)
{
	mAxisPoints[xAxis][StartPoint] = bounds.left;
	mAxisPoints[xAxis][EndPoint] = bounds.left + bounds.width;
	mAxisPoints[yAxis][StartPoint] = bounds.top;
	mAxisPoints[yAxis][EndPoint] = bounds.top + bounds.height;
}

float* SolidObject::getPointAddress(int dim, int point)
{
	return &(mAxisPoints[dim][point]);
}

