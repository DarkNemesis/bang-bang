#include "Utility.h"

void windowScale(Vector2u targetSize, Sprite& targetSprite)
{
	targetSprite.setScale(targetSize.x / targetSprite.getLocalBounds().width, targetSize.y / targetSprite.getLocalBounds().height);
}

FloatRect rectShift(FloatRect target, FloatRect dest)
{
	return FloatRect(target.left + dest.left, target.top + dest.top, target.width, target.height);
}

FloatRect rectShift(IntRect target, FloatRect dest)
{
	return FloatRect(target.left + dest.left, target.top + dest.top, target.width, target.height);
}

FloatRect rectIntToFloat(IntRect target)
{
	return FloatRect(target.left, target.top, target.width, target.height);
}

void centerOrigin(Animation& animation)
{
	sf::FloatRect bounds = animation.getGlobalBounds();
	animation.setOrigin(std::floor(bounds.width / 2.f), std::floor(bounds.height / 2.f));
}

bool collision(const SceneNode& lhs, const SceneNode& rhs)
{
	vector<FloatRect> lhsRect, rhsRect;
	lhs.getCollisionRect(lhsRect);
	rhs.getCollisionRect(rhsRect);
	auto ltr = lhsRect.begin();
	auto rtr = rhsRect.begin();
	//for (auto ltr : lhsRect)
	//	for (auto rtr : rhsRect)
			if ((ltr)->intersects(*rtr))
				return true;
	return false;	
}

bool collision(FloatRect lhsRect, const SceneNode& rhs)
{
	vector<FloatRect> rhsRect;
	rhs.getCollisionRect(rhsRect);
	for (auto rtr : rhsRect)
			if (lhsRect.intersects(rtr))
				return true;
	return false;
}