#pragma once
#include <map>
#include <memory>
#include <string>
using namespace std;

template <typename Resource, typename Identifier>
class ResourceHolder
{
	map<Identifier, unique_ptr<Resource>> mResourceMap;	
public:	
	void load(Identifier ID, const string& filename)
	{
		unique_ptr<Resource> resource(new Resource());
		resource->loadFromFile(filename);
		mResourceMap.insert(std::make_pair(ID, std::move(resource)));
	}
	Resource& get(Identifier ID)
	{
		auto found = mResourceMap.find(ID);
		return *found -> second;
	}
};

