#include "Block.h"
#include "Opponent.h"
#include "PowerUp.h"
#include "Tank.h"
#include "Animal.h"
#include "Player.h"
#include "Platform.h"

Platform* Block::mPlatformPointer;

void Block::issuePowerUpCommand(Vertex* quad)
{
	Vector2f position = quad[0].position;
	Vector2f size = quad[2].position - quad[0].position;
	Vector2f dest = Vector2f(position.x, position.y - size.y);

	Command power;
	power.mCategory = Category::Objects;
	power.mAction = [dest, this](SceneNode& node, Time dt){Objects& power = static_cast<Objects&>(node); power.createPowerUp(mPowerId, dest); };
	SceneNode::mCommandQueue.push(power);
	//mAttributeBit -= (1 << PowerBit);
}

Block::Block(int block, int x, int y, int blockNo) : mPosition(x, y), mId(block), mBlockNumber(blockNo)
{	
	isBouncing = false;
}

Block* Block::createNewBlock(int blockID, int x, int y, int blockNo)
{
	switch (blockID)
	{
	case 1:
	case 2:
	case 3:
	case 4:		return(new Block(blockID, x, y, blockNo));
	case 5:		return(new PowerBlock(blockID, x, y, blockNo));
	case 6:
	case 7:
	case 8:
	case 9:		return(new Spawner(blockID, x, y, blockNo));
	case 10:	return(new Portal(blockID, x, y, blockNo));
	default:	return(new Block(blockID, x, y, blockNo));
	}
}

Block::~Block()
{
}

void Block::setID(int id)
{
	mId = id;	
}
int Block::getID()
{
	return mId;
}

int Block::getBlockNumber()
{
	return mBlockNumber;
}

bool Block::issueGreenKey()
{	
	return true;
}

Vector2i Block::getPosition()
{
	return mPosition;
}

void Block::collidedWith(Entity *entity, bitset<8> polyRect)
{	
	Animal* animal = dynamic_cast<Animal*>(entity);
	if (animal && polyRect[MiddleLeft])
		entity->setVelocity(abs(entity->getVelocity().x), entity->getVelocity().y);
	else if (animal && polyRect[MiddleRight])
		entity->setVelocity(-abs(entity->getVelocity().x), entity->getVelocity().y);
	else
		entity->setVelocity(0, entity->getVelocity().y);

	entity->setAcceleration(0, entity->getAcceleration().y);

	if (polyRect[MiddleLeft])
		entity->setPosition(mVertexPointer[1].position.x, entity->getPosition().y);
	else if (polyRect[MiddleRight])
		entity->setPosition(mVertexPointer[0].position.x - entity->getSize().x - 1, entity->getPosition().y);
}

void Block::steppedOn(Entity *entity)
{
	entity->standOnSolidGround();
	entity->setPosition(entity->getPosition().x, mVertexPointer->position.y - entity->getSize().y);
	entity->setVelocity(Vector2f(entity->getVelocity().x, min(0.f, entity->getVelocity().y)));

	Animal* animal = dynamic_cast<Animal*>(entity);
	if (animal && isBouncing)
		animal->goInRepairMode();
}

void Block::bouncedOff(Entity *entity)
{
	if (dynamic_cast<Tank*>(entity))
	{
		entity->setPosition(entity->getPosition().x, mVertexPointer[2].position.y);
		entity->setVelocity(Vector2f(entity->getVelocity().x, 0.f));

		mPlatformPointer->bounceTile(this);
	}
}

void Block::updateTexCords()
{
	sf::Vertex* quad = mVertexPointer;
	int count = 10;
	int width = 64;
	int height = 36;
	if (mId >= 1)
	{
		int row = ((mId - 1) / count) * height;
		int column = ((mId - 1) % count) * width;
		quad[0].texCoords = Vector2f(column, row);
		quad[1].texCoords = Vector2f(column + width, row);
		quad[2].texCoords = Vector2f(column + width, row + height);
		quad[3].texCoords = Vector2f(column, row + height);
	}
	else
	{		
		quad[0].texCoords = Vector2f(0, 0);
		quad[1].texCoords = Vector2f(0, 0);
		quad[2].texCoords = Vector2f(0, 0);
		quad[3].texCoords = Vector2f(0, 0);
	}
}

/********************************
		SPAWNER Definitions
********************************/

Spawner::Spawner(int blockID, int x, int y, int blockNo) : Block(blockID, x, y, blockNo)
{
	Command cmd;
	cmd.mCategory = Category::Opponent;
	cmd.mAction = [this](SceneNode& node, Time dt)
	{Opponent& Node = static_cast<Opponent&>(node); Node.addSpawner(this); };
	mPlatformPointer->mCommandQueue.push(cmd);
}

void Spawner::steppedOn(Entity *entity)
{
	Spawner::collidedWith(entity, 0);
}

void Spawner::bouncedOff(Entity *entity)
{
	Spawner::collidedWith(entity, 0);
}

void Spawner::collidedWith(Entity *entity, bitset<8> polyRect)
{
	Tank* player = dynamic_cast<Tank*>(entity);
	if (player)
	{
		Command spawnComm;
		spawnComm.mCategory = Category::Player;
		spawnComm.mAction = [this]
			(SceneNode& node, Time dt)	{Player& player = static_cast<Player&>(node); player.checkAndUseKey(this); };
		mPlatformPointer->mCommandQueue.push(spawnComm);
	}
}

int Spawner::getStatus()
{
	return (mId - 6);
}

void Spawner::activateKey()
{
	if (mId == 8 || mId == 9)
	{

		mVertexPointer[0].position = Vector2f(0, 0);
		mVertexPointer[1].position = Vector2f(0, 0);
		mVertexPointer[2].position = Vector2f(0, 0);
		mVertexPointer[3].position = Vector2f(0, 0);
	}
	else
	{
		mId++;
	}
	updateTexCords();
}

array<Vector2f, 2> Spawner::getSpawnInfo()
{
	array<Vector2f, 2> spawnInfo;

	int prob = (mPosition.y / 29.f) * 100;
	default_random_engine generator;
	uniform_int_distribution<int> distribution(0, 100);

	//Spawn Left Side
	if (distribution(generator) <= prob)
	{
		spawnInfo[0] = Vector2f(mVertexPointer[3].position);
		spawnInfo[1] = Vector2f(-1, 0);
	}
	else
	{
		spawnInfo[0] = Vector2f(mVertexPointer[2].position);
		spawnInfo[1] = Vector2f(+1, 0);
	}
	return spawnInfo;
}

/********************************
		PORTAL Definitions
********************************/

Portal::Portal(int blockID, int x, int y, int blockNo) : Block(blockID, x, y, blockNo)
{	
}

void Portal::steppedOn(Entity *entity)
{
	Portal::collidedWith(entity, 0);
}

void Portal::bouncedOff(Entity *entity)
{
	Portal::collidedWith(entity, 0);
}

void Portal::collidedWith(Entity *entity, bitset<8> polyRect)
{
	Animal* animal = dynamic_cast<Animal*>(entity);
	if (animal)
	{
		Command spawnComm;
		spawnComm.mCategory = Category::Opponent;
		spawnComm.mAction = [this, entity]
			(SceneNode& node, Time dt)	{Opponent& opp = static_cast<Opponent&>(node); opp.warp(entity); };
		mPlatformPointer->mCommandQueue.push(spawnComm);
	}
}

/********************************
	POWER BLOCK Definitions
********************************/

PowerBlock::PowerBlock(int blockID, int x, int y, int blockNo) : Block(blockID, x, y, blockNo)
{
}

void PowerBlock::bouncedOff(Entity *entity)
{
	Block::bouncedOff(entity);
}
