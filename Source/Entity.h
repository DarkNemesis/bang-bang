#pragma once
#include "SceneNode.h"
#include "SoundPlayer.h"
#include "Physics.h"
#include "Animation.h"

enum CollisionBoxes{Complete, Upper, Lower, MiddleLeft, MiddleRight, CollisionBoxesCount};

class Entity : public SceneNode, public Physics
{	
protected:
	bool		sDead;
	virtual void	updateCurrent(Time dt);
	array<FloatRect, CollisionBoxesCount>	mCollisionRects;
public:
					Entity();	
	virtual void	handleCollision(SceneNode* collider, bitset<8> polyRect);
	virtual void	adaptPosition();	
	virtual bool	isMarkedForRemoval();
	virtual bool	isDestroyed();
	virtual void	destroy();
};

