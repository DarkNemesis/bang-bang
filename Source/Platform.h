#pragma once
#include "Block.h"
#include <map>

extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

class Platform : public SceneNode
{	
	vector<Block*>	mBlocks;

	map<Block*, int> mBounceStack;

	Texture		mTexture;
	VertexArray	mVertices;

	Vector2f	mBlockSize;
	Vector2f	mBlockCount;
	
	vector<int>	mBlockKeys;
	vector<int>	mPowerBlocks;

public:
	Platform();
	~Platform(void);
	Vector2f getWorldSize();	

	unsigned int getCategory() const;	

	void	drawCurrent(RenderTarget& target, RenderStates states) const;
	void	updateCurrent(Time dt);

	void	bounceTile(Block* block);
	void	loadLevel(int levelNo);
	void	checkNodeCollision(SceneNode& node, set<Pair>& collisionPairs);
	void	generateGreenKey();

	int			convertPositionToBlockNo(Vector2f position);
	Vector2f	convertBlockNoToPosition(int blockNo);
	int			mapBlockToGrid(int blockNo);
};