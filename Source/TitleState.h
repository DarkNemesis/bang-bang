#pragma once
#include "State.h"
#include <SFML/Graphics.hpp>
using namespace sf;
class TitleState :
	public State
{
	Sprite mSprite;
	RectangleShape mRect;
	int mFade;
	float mFadeValue;
public:
	TitleState(StateStack& Stack, Context context);
	~TitleState(void);
	virtual void draw();
	virtual bool update(Time dt);
	virtual bool processEvent(Event& event);
};

