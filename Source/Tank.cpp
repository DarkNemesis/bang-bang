#include "Tank.h"
#include "Block.h"
#include "Opponent.h"
#include "PowerUp.h"
#include "Player.h"

Tank::Tank() 
: mExplosion(Animation(mTextures->get(Textures::TankExplosion), IntRect(0, 0, 100, 100)))
{
	mDoesItCollide = true;

	Physics::mDrag = drag(0.9, 0.0005);
	
	float factorX = SceneNode::mWindowSize.x / 1366.f;
	float factorY = SceneNode::mWindowSize.y / 768.f;
	mScale = Vector2f(1, 1); 
	
	mTextures->get(Textures::Tank).setSmooth(true);
	mSprite.setTexture(mTextures->get(Textures::Tank));
	mSprite.setTextureRect(IntRect(0, 0, 55, 76));
	mSprite.scale(mScale);
	
	// Preparing texture
	mTextures->get(Textures::TankExplosion).setSmooth(true);
		
	// Seeding walking animation
	centerOrigin(mExplosion);
	mExplosion.setNumFrames(15);
	mExplosion.setRepeating(false);
	mExplosion.setDuration(seconds(1));
	mExplosion.setScale(mScale.x / 1, mScale.y / 1);

	setCollRect();
	mCategory = Category::TankA;
}

void Tank::updateCurrent(Time dt)
{
	if (!sDead)
		Entity::updateCurrent(dt);
	else 
		mExplosion.update(dt);	

	SolidObject::update(getWorldTransform().transformRect(mSprite.getGlobalBounds()));
}

void Tank::jump()
{
	if (mVelocity.y == 0 && mAcceleration.y == 0)
		mVelocity.y = -775;
}

void Tank::destroy()
{
	if(!sDead)
		move(0, getSize().y / -2);
	sDead = true;	
}

bool Tank::isMarkedForRemoval()
{
	return (mExplosion.isFinished() && sDead);
}

void Tank::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (!sDead)
		target.draw(mSprite, states);
	else 
		target.draw(mExplosion, states);	
}

Vector2f Tank::getSize()
{
	return Vector2f(mSprite.getGlobalBounds().width, mSprite.getGlobalBounds().height);
}

void Tank::handleCollision(SceneNode* collider, bitset<8> polyRect)
{
}

void Tank::accelerate(Vector2f velocity)
{
	mAcceleration.x = velocity.x;

	if (mAcceleration.x > 0)
		mSprite.setTextureRect(IntRect(0, 0, 55, 76));
	else if (mAcceleration.x < 0)
		mSprite.setTextureRect(IntRect(55, 0, -55, 76));
}

void Tank::getCollisionRect(vector<FloatRect>& rectangles) const
{
	rectangles.clear();
	FloatRect bounds = getWorldTransform().transformRect(mSprite.getGlobalBounds());
	for (int i = 0; i < CollisionBoxesCount; i++)
		rectangles.push_back(rectShift(mCollisionRects[i], bounds));
}

void Tank::setCollRect()
{
	mCollisionRects[Complete] = FloatRect(0.5 * mScale.x, 0 * mScale.y, 54.2 * mScale.x, 76 * mScale.y);
	mCollisionRects[Lower]		= FloatRect(13 * mScale.x, 60 * mScale.y, 29 * mScale.x, 15.7 * mScale.y);
	
	mCollisionRects[MiddleLeft]	= FloatRect(0.5 * mScale.x, 18 * mScale.y, 27.5 * mScale.x, 43 * mScale.y);
	mCollisionRects[MiddleRight]	= FloatRect(28 * mScale.x, 18 * mScale.y, 26.7 * mScale.x, 43 * mScale.y);	
	mCollisionRects[Upper]		= FloatRect(3.7 * mScale.x, 0 * mScale.y, 47.5 * mScale.x, 18 * mScale.y);
}

Tank::~Tank()
{

}
