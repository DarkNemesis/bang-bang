#pragma once
#include <SFML/Graphics.hpp>
#include <deque>
#include "State.h"
#include "GUI.h"
#include "fstream"
using namespace sf;
class MenuState : public State
{
	deque<GUI::Container> mContainer;
	Sprite mSprite;
	Text mTitle;
	Text mPlayerText;
public:
	MenuState(StateStack& stack, Context context);
	~MenuState(void);
	virtual void draw();
	virtual bool update(Time dt);
	virtual bool processEvent(Event& event);
};

