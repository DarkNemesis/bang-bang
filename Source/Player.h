#pragma once
#include <SFML/Graphics.hpp>
#include "Command.h"
#include "Tank.h"
#include "Identifiers.h"
#include "Block.h"
class GameHUD;
enum LevelKeys { Red, Yellow, Green, Blue, LKCount };
class Player : public SceneNode
{
	int		mPlayerLife;
	float	mSpawnClock;
	float	mSpawnDelay;
	
	array<int, LevelKeys::LKCount>	mKeyQuantity;
	Vector2f	mSpawnPosition;
	SceneNode*	mPlayerNode;
public:
	friend class GameHUD;
	Player();
	~Player(void);
	function<void()> mWorldTrigger;
	void		updateCurrent(Time dt);
	void		setSpawnPosition(Vector2f position);
	SceneNode*	getPlayerNode();
	void		addKey(unsigned int key);
	void		checkAndUseKey(Spawner* spawner);
};

class PlayerInput
{
public:
	void	handleEvent(const Event& event, CommandQueue& commands);
	void	handleRealtimeInput(CommandQueue& commands);
};

struct TankMover
{
	Vector2f velocity;
	TankMover(float x, float y) : velocity(x,y) {}
	void operator() (SceneNode& node, Time dt)
	{
		Tank& tank = static_cast<Tank&>(node);
		tank.accelerate(velocity);
	}
};
		