#include "MusicPlayer.h"


MusicPlayer::MusicPlayer(void)
{
	mFilenames[MusicTheme::MenuTheme] = "Audio/MenuTheme.ogg";
	mFilenames[MusicTheme::GameTheme] = "Audio/GameTheme.ogg";
	mVolume = 100.f;
}


MusicPlayer::~MusicPlayer(void)
{
}

void MusicPlayer::play(MusicTheme::MusicTheme musicID)
{
	string filename = mFilenames[musicID];
	mMusic.openFromFile(filename);
	mMusic.setVolume(mVolume);
	mMusic.setLoop(true);
	mMusic.play();
}
