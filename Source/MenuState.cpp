#include "MenuState.h"
#include "Utility.h"

MenuState::MenuState(StateStack& stack, Context context) : State(stack, context)
{
	mSprite.setTexture(context.mTextures -> get(Textures::MenuScreen));
	windowScale(context.mWindow -> getSize(), mSprite);	
	
	vector<Texture*> textures{ &(*context.mTextures).get(Textures::ButtonNormal), &(*context.mTextures).get(Textures::ButtonActive), &(*context.mTextures).get(Textures::ButtonSelected) };
	vector<Color> colors{ Color(102, 0, 0), Color::Black, Color::Yellow };

	GUI::Container mainContainer(*context.mWindow);

	GUI::Button* playButton = new GUI::Button(GUI::Button::TexLabeled);
		playButton->setAction([this]() {requestStackPop(); requestStackPush(States::Game); });
		playButton->setPosition(Vector2f(context.mWindow->getSize().x / 2.f, 350));
		playButton->setTexture(textures);
		playButton->setColors(GUI::Button::Label, colors);
		playButton->setLabel("Play", (*context.mFonts).get(Fonts::MenuButton), 30);
		mainContainer.pack(playButton);

	GUI::Button* UserButton = new GUI::Button(GUI::Button::TexLabeled);
		UserButton->setPosition(Vector2f(context.mWindow->getSize().x / 2.f, 430));
		UserButton->setTexture(textures);
		UserButton->setColors(GUI::Button::Label, colors);
		UserButton->setLabel("User", (*context.mFonts).get(Fonts::MenuButton), 30);
		UserButton->setAction([this, context, textures]()
			{
				GUI::Container userContainer(*context.mWindow);

				fstream fin("User\\UserData.bin", ios::in);
				int position = 400;
				while (!fin.eof())
				{
					string username;
					std::getline(fin, username);
					if (!username.empty())
					{
						GUI::Button* PlayerButton = new GUI::Button(GUI::Button::TexLabeled);
							PlayerButton->setPosition(Vector2f(context.mWindow->getSize().x / 2.f, position));
							PlayerButton->setTexture(textures);
							PlayerButton->setLabel(username, (*context.mFonts).get(Fonts::MenuButton), 30);
							PlayerButton->setAction([] () {});
							userContainer.pack(PlayerButton);
						position += 50;
					}					
				}

				GUI::Button* BackButton = new GUI::Button(GUI::Button::TexLabeled);
					BackButton->setPosition(Vector2f(context.mWindow->getSize().x / 2.f, position + 50));
					BackButton->setTexture(textures);
					BackButton->setLabel("Back", (*context.mFonts).get(Fonts::MenuButton), 30);
					BackButton->setAction([this]() {mContainer.pop_front(); });
					userContainer.pack(BackButton);

				mContainer.push_front(userContainer);
			});
		mainContainer.pack(UserButton);
	
	GUI::Button* exitButton = new GUI::Button(GUI::Button::TexLabeled);
		exitButton->setAction([this]() {requestStackPop();});
		exitButton->setPosition(Vector2f(context.mWindow->getSize().x / 2.f, 510));
		exitButton->setTexture(textures);
		exitButton->setColors(GUI::Button::Label, colors);
		exitButton->setLabel("Exit", (*context.mFonts).get(Fonts::MenuButton), 30);
		mainContainer.pack(exitButton);
	
	mContainer.push_front(mainContainer);

	if (context.mUser -> mRegister == false)
	{
		GUI::Container textContainer(*context.mWindow);
		shared_ptr<string> username = make_shared<string>();
		GUI::TextBox *userText = new GUI::TextBox(Vector2f(200, 200), Vector2f(500, 32), username);

		userText -> setString("Enter Your Name");
		userText -> setFont((*context.mFonts).get(Fonts::MenuButton));
		userText -> setBackColor(Color::White, Color::Black);
		userText -> setColor(Color::Red);
		userText -> setKeyLength(15);
		userText -> setFunction([this, username, context] ()
					{
						//Checking if the new user already exist
						fstream fcheck("User\\" + *username + ".bin", ios::in);

						//If it doesn't exist
						if (fcheck.fail())
						{
							//Storing the new user in file
							fstream fin("User\\UserData.bin", ios::in);
							fin.seekp(0, fin.end);
							int length = fin.tellp();
							fin.seekg(0, fin.beg);

							char *fileData = new char [length];							
							fin.read(fileData, length);
							fin.flush();
							fin.close();

							fstream fout("User\\UserData.bin", ios::out | ios::trunc);
							fout <<  *username << "\n";
							fout.write(fileData, length);							
							fout.close();

							//Creating a new user file
							fout.open("User\\" + *username + ".bin", ios::in | ios::out | ios::trunc);
							fout << "5\n0\n1\n1";
							context.mUser -> setBaseValue(5,0,1,1);
						}
						
						//If it exists
						else
							context.mUser -> fileInput("User\\" + *username + ".bin");
						
						//Register this user as the current User
						context.mUser -> mUserName = *username;
						context.mUser -> mRegister = true;

						//Display the new User
						mPlayerText.setString("Welcome " + *username);
						centerOrigin(mPlayerText);
						mPlayerText.setPosition(context.mWindow -> getSize().x / 2.f, 250);

						//Remove the text Box
						mContainer.pop_front();
					});

		textContainer.pack(userText);
		mContainer.push_front(textContainer);
	}

	context.mMusic -> play(MusicTheme::MenuTheme);
		
	mTitle.setFont(context.mFonts -> get(Fonts::MenuTitle));
	mPlayerText.setFont(context.mFonts -> get(Fonts::MenuTitle));

	mTitle.setString("Bang Bang");
	mPlayerText.setString("Welcome " + context.mUser -> mUserName);

	mTitle.setCharacterSize(100);
	mPlayerText.setCharacterSize(60);

	mTitle.setColor(Color::Black);
	mPlayerText.setColor(Color(200, 200, 200));

	centerOrigin(mTitle);
	centerOrigin(mPlayerText);

	mTitle.setPosition(context.mWindow -> getSize().x / 2.f, 100);
	mPlayerText.setPosition(context.mWindow -> getSize().x / 2.f, 250);
}


MenuState::~MenuState(void)
{}

bool MenuState::update(sf::Time)
{
	(mContainer[0]).update();
	return true;
}

bool MenuState::processEvent(Event& event)
{
	if (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
		requestStackPop();
	(mContainer[0]).handleEvent(event);
	return false;
}

void MenuState::draw()
{
	sf::RenderWindow& window = *getContext().mWindow;
	window.clear();
	window.draw(mSprite);
	window.draw(mTitle);
	window.draw(mPlayerText);
	window.draw((mContainer[0]));
}

