#pragma once
#include <list>
#include <unordered_set>
#include "Physics.h"
#include "SceneNode.h"
typedef pair<SolidObject*, float*> box;
class Collision
{
	static array<list<box>, 2> mDimensionArray;
	vector<pair<SolidObject*, SolidObject*>> mCollisionPair;
public:
	void sort();
	static void registerSolidObject(SolidObject* a);
	static void deleteSolidObject(SolidObject* a);
	void sweepAndPrune();
	void getCollisionPair(set<SceneNode::Pair>& pairs);
};