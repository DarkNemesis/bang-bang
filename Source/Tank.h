#pragma once
#include <array>
#include "Entity.h"
#include "ResourceHolder.h"
#include "Animation.h"

class Tank : public Entity, public SolidObject
{
	Sprite		mSprite;
	Animation	mExplosion;
	Vector2f	mScale;
	
	
	void				setCollRect();
public:
						Tank();
						~Tank();

	//Update and Draw functions
	virtual void		drawCurrent(RenderTarget& target, RenderStates states) const;	
	virtual void		updateCurrent(Time dt);
	
	void				accelerate(Vector2f velocity);
	virtual void		jump();
		
	Vector2f			getSize();
	virtual void		getCollisionRect(vector<FloatRect>& rectangles) const;
	void				handleCollision(SceneNode* collider, bitset<8> polyRect);
	
	//Destuction functions
	virtual void		destroy();
	virtual bool		isMarkedForRemoval();
};

