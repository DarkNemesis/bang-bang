#pragma once
#include <SFML/Graphics.hpp>
#include <array>
using namespace std;
using namespace sf;

struct drag
{
	double k1, k2;
	drag(double ka, double kb)
	{
		k1 = ka;
		k2 = kb;
	}
};

class Physics
{
protected:
	Vector2f	mVelocity;
	Vector2f	mAcceleration;
	drag		mDrag;
	bool		isStandingOnSolidGround;
public:
	const static double cGravity;
	
	Physics();
	~Physics();
	
	double		computeDrag(double velocity);	
	void		applyPhysics(Time dt);

	Vector2f	getVelocity();
	void		setVelocity(Vector2f velocity);
	void		setVelocity(float x, float y);

	Vector2f	getAcceleration();
	void		setAcceleration(Vector2f acceleration);
	void		setAcceleration(float x, float y);

	void			standOnSolidGround();
	virtual void	jump();
};

enum Point { StartPoint, EndPoint };
enum Axis { xAxis, yAxis };
class SolidObject
{
protected:
	array<array<float, 2>, 2>	mAxisPoints;
public:
	bool smallerThan(SolidObject a);
	float* getPointAddress(int dim, int point);
	SolidObject();
	~SolidObject();
	void update(FloatRect bounds);
};



