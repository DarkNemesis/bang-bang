#include "Animation.h"

Animation::Animation(const Texture& texture, IntRect frameRect) :	mFrameRect(frameRect), mSprite(texture, frameRect), 
																	mFrames(0), mCurrentFrame(0), mDuration(Time::Zero),
																	mElapsedTime(Time::Zero), mRepeat(false)
{}

Animation::Animation() :	mFrameRect(), mFrames(0), mCurrentFrame(0),	mDuration(Time::Zero),
							mElapsedTime(Time::Zero), mRepeat(false)
{}

void Animation::setTexture(const Texture& texture)
{
	mSprite.setTexture(texture);
}

const Texture* Animation::getTexture() const
{
	return mSprite.getTexture();
}

void Animation::setFrameRect(IntRect frameRect)
{
	mFrameRect = frameRect;
}

IntRect Animation::getFrameRect() const
{
	return mFrameRect;
}

void Animation::setNumFrames(size_t numFrames)
{
	mFrames = numFrames;
}

size_t Animation::getNumFrames() const
{
	return mFrames;
}

void Animation::setDuration(Time duration)
{
	mDuration = duration;
}

Time Animation::getDuration() const
{
	return mDuration;
}

void Animation::setRepeating(bool flag)
{
	mRepeat = flag;
}

bool Animation::isRepeating() const
{
	return mRepeat;
}

void Animation::restart()
{
	mCurrentFrame = 0;
}

bool Animation::isFinished() const
{
	return mCurrentFrame >= mFrames;
}

FloatRect Animation::getLocalBounds() const
{
	return mSprite.getLocalBounds();	
}

FloatRect Animation::getGlobalBounds() const
{
	return getTransform().transformRect(getLocalBounds());
}

void Animation::update(Time dt)
{
	Time timePerFrame = mDuration / float(mFrames);
	Vector2i textureBounds(mSprite.getTexture() -> getSize());
	IntRect textureRect = mSprite.getTextureRect();

	if (mCurrentFrame == 0)
		textureRect = mFrameRect;
	
	// While we have a frame to process
	for (mElapsedTime += dt; mElapsedTime >= timePerFrame && (mCurrentFrame <= mFrames || mRepeat); mElapsedTime -= timePerFrame)
	{
		// Move the texture rect left
		textureRect.left += textureRect.width;

		// If we reach the end of the texture
		if (textureRect.left + textureRect.width > textureBounds.x)
		{
			// Move it down one line
			textureRect.left = 0;
			textureRect.top += textureRect.height;
		}

		// And progress to next frame		
		if (mRepeat)
		{
			mCurrentFrame = (mCurrentFrame + 1) % mFrames;
			if (mCurrentFrame == 0)
				textureRect = mFrameRect;
		}
		else
			mCurrentFrame++;
		if (isFinished())
			textureRect = IntRect(0,0,0,0);
	}
	mSprite.setTextureRect(textureRect);
}

void Animation::draw(RenderTarget& target, RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(mSprite, states);
}