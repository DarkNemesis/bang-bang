#pragma once
#include <SFML/Graphics.hpp>
#include <set>
#include "Opponent.h"
#include "ResourceHolder.h"
#include "SceneNode.h"
#include <array>
#include "Tank.h"
#include "Command.h"
#include "GameHUD.h"
#include "Animal.h"
#include "PowerUp.h"
#include "Player.h"
#include "Collision.h"
#include "Platform.h"

enum EnemySpawn { EnemyCount, SpawnRequest, SpawnCount, DestroyCount};
enum Layer { BackGround, MiddleGround, ForeGround, LayerCount};
enum UnitHierarchy { PlayerUnit, ArtificialUnit, EnemyUnit, UnitCount };

class GameView
{
	View mView;
	//SceneNode* mCenterNode;
	Vector2f mWindowSize;
	Vector2f mWorldSize;
	Vector2f mUpperBoundary, mLowerBoundary;
public:
	friend class World;
	void update(SceneNode* node);
	void reset(SceneNode* node);
	Vector2f getCenter();
	void setWindowSize(Vector2f size);
};

class World
{	
	struct WorldPointers
	{
		SpriteNode* mSpriteNode;
		Player*		mPlayer;
		GameHUD*	mHUD;
		Platform*	mPlatform;
		Opponent*	mOpponent;
	}mWorldPointers;
	int				mLevel;
	RenderWindow&	mWindow;
	TextureHolder	mTextures;

	Collision		mCollisionList;
		
	SceneNode mSceneGraph;
	array<SceneNode*, LayerCount> mSceneLayers;	
	array<SceneNode*, UnitCount> mSceneUnits;	

	GameView mGameView;		
public:
					World(RenderWindow& window);
					function<void()> mDestroy;
	void			loadTexture();
	void			buildScene(int level);
	void			draw();
	void			update(Time dt);
	void			handleCollision();
	CommandQueue&	getCommandQueue();
	void			endLevel(int n);
};

