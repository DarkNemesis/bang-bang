#include "Collision.h"

array<list<box>, 2> Collision::mDimensionArray;

void Collision::sort()
{
	mDimensionArray[xAxis].sort([](const box& a, const box& b){return (*a.second < *b.second); });
	mDimensionArray[yAxis].sort([](const box& a, const box& b){return (*a.second < *b.second); });
}

void Collision::registerSolidObject(SolidObject* a)
{
	mDimensionArray[xAxis].push_back(make_pair(a, a->getPointAddress(xAxis, StartPoint)));
	mDimensionArray[xAxis].push_back(make_pair(a, a->getPointAddress(xAxis, EndPoint)));

	mDimensionArray[yAxis].push_back(make_pair(a, a->getPointAddress(yAxis, StartPoint)));
	mDimensionArray[yAxis].push_back(make_pair(a, a->getPointAddress(yAxis, EndPoint)));
}

void Collision::deleteSolidObject(SolidObject* a)
{
	mDimensionArray[xAxis].remove_if([a](const box& v){ return (v.first == a); });
	mDimensionArray[yAxis].remove_if([a](const box& v){ return (v.first == a); });
}

void Collision::sweepAndPrune()
{
	unordered_set<SolidObject*> temp;
	vector<pair<SolidObject*, SolidObject*>> xCollPair;
		
	sort();
	
	for (auto itr : mDimensionArray[xAxis])
	{
		if (temp.insert(itr.first).second == false)
			temp.erase(itr.first);
		else
			for (auto jtr : temp)
				if (jtr != itr.first)
				{
					SolidObject* Min = min(jtr, itr.first);
					SolidObject* Max = max(jtr, itr.first);
					xCollPair.push_back(make_pair(Min, Max));
				}
			
	}

	temp.clear();
	for (auto itr : mDimensionArray[yAxis])
	{
		if (temp.insert(itr.first).second == false)
			temp.erase(itr.first);
		else
			for (auto jtr : temp)
				if (jtr != itr.first)
				{
					SolidObject* Min = min(jtr, itr.first);
					SolidObject* Max = max(jtr, itr.first);
					if (find(xCollPair.begin(), xCollPair.end(), make_pair(Min, Max)) != xCollPair.end())
						mCollisionPair.push_back(make_pair(Min, Max));
				}
	}	
}

void Collision::getCollisionPair(set<SceneNode::Pair>& pairs)
{
	for (auto itr : mCollisionPair)
	{
		pairs.insert(make_pair((SceneNode*)(itr.first), (SceneNode*)(itr.second)));
	}
}