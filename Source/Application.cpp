#include "Application.h"


Application::Application(void) : mStateStack(State::Context(mWindow, mTextures, mPlayerInput, mMusic, mFonts, mSound, mUser))
{
	vector<VideoMode> mode = VideoMode::getFullscreenModes();
	mMode = mode[0];
	mMode = VideoMode::getDesktopMode();
	mWindow.create(mMode, "BonkTank", Style::Fullscreen);
	mWindow.create(mMode, "BonkTank");
	mFrameTime = seconds(1.0/60.0);
	registerStates();
	mStateStack.pushState(States::States::Title);

	mApplicationIcon.loadFromFile("Images/Icon.png");
	mWindow.setIcon(32, 32, mApplicationIcon.getPixelsPtr());
}


Application::~Application(void)
{
}

void Application::run()
{
	Time ElapsedTime, UpdatedTime;
	Clock clock;

	Clock FPS;
	int frame = 0;

	while(mWindow.isOpen())
	{
		ElapsedTime = clock.restart();
		UpdatedTime += ElapsedTime;
		while(UpdatedTime > mFrameTime)
		{
			processEvent();
			
			update(mFrameTime);
			
			UpdatedTime -= mFrameTime;
			if(mStateStack.isEmpty())
				mWindow.close();
		}
		
		render(); 
		frame++;
		if (FPS.getElapsedTime().asSeconds() >= 1)
		{
			cout << "FPS: " << frame << endl;
			frame = 0;
			FPS.restart();
		}		
	}
}

void Application::update(Time dt)
{
	mStateStack.update(dt);
}

void Application::processEvent()
{
	Event event;
	while(mWindow.pollEvent(event))
	{
		mStateStack.processEvent(event);
		if (event.type == Event::EventType::Closed)
			mWindow.close();
	}
}

void Application::render()
{
	mWindow.clear();
	mStateStack.draw();
	mWindow.display();
}

void Application::registerStates()
{
	mStateStack.registerState<TitleState>(States::States::Title);
	mStateStack.registerState<MenuState>(States::States::Menu);
	mStateStack.registerState<GameState>(States::States::Game);
	mStateStack.registerState<LoadingState>(States::States::Load);
}