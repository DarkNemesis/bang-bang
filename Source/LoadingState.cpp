#include "LoadingState.h"


LoadingState::LoadingState(StateStack& stack, Context context) : State(stack, context), mTask(context), mWindow(*getContext().mWindow)
{
	context.mTextures->load(Textures::LoadingScreen, "Images/LoadingScreen.png");
	context.mTextures -> get(Textures::LoadingScreen).setSmooth(true);
	mSprite.setTexture(context.mTextures -> get(Textures::LoadingScreen));
	
	centerOrigin(mSprite);
	windowScale(mWindow.getSize(), mSprite);	
	mSprite.setPosition(mWindow.getSize().x / 2.f, mWindow.getSize().y / 2.f);

	mFill.setSize(Vector2f(mWindow.getSize().x * 0.1, mWindow.getSize().y * 0.3));
	centerOrigin(mProgress);

	mTask.execute();
}

LoadingState::~LoadingState(void)
{
}

void LoadingState::draw()
{
	mWindow.clear();
	mWindow.draw(mSprite);	
}

bool LoadingState::processEvent(Event& event)
{
	return true;
}

bool LoadingState::update(Time dt)
{
	if(mTask.isFinished())
	{
		mTask.joinRequest();
		requestStackPop();
		requestStackPush(States::Menu);
	}
	return true;
}