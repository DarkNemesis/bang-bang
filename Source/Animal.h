#pragma once
#include "Entity.h"
#include <array>
#include <fstream>


class Animal : public Entity, public SolidObject
{	
protected :
	map<Modes::Modes, unique_ptr<Animation>> mAnimation;
	Modes::Modes mCurrentMode;
	Time	mDeathTimer;
	Clock	mDeathClock;
	double	mHaltVelocity;
	bool	mHaveKey;
	virtual void			setCollRect() = 0;
public:
							Animal();
							~Animal(void);
	virtual void			drawCurrent(RenderTarget& target, RenderStates states) const;
	virtual unsigned int	getCategory() const;
	virtual void			updateCurrent(Time dt);
	void					goInRepairMode();
	void					recoverFromRepair();
	Modes::Modes			getCurrentMode();
	Vector2f				getSize();
	void					giveKey();
	bool					haveKey();
	void					handleCollision(SceneNode* collider, bitset<8> polyRect);
	virtual void			getCollisionRect(vector<FloatRect>& rectangles) const;
};

class Wolf : public Animal
{
	void					setCollRect();
public :
							Wolf(TextureHolder& textures);	
};

