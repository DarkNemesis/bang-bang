#include "World.h"
#include <fstream>

bool matchCategory(SceneNode::Pair& pair, Category::Category type1, Category::Category type2)
{
	unsigned int category1 = pair.first -> getCategory();
	unsigned int category2 = pair.second -> getCategory();
	if (type1 & category1 && type2 & category2)
		return true;
	else if (type1 & category2 && type2 & category1)
	{
		swap(pair.first, pair.second);
		return true;
	}
	else return false;
}

void GameView::setWindowSize(Vector2f size)
{
	mWindowSize = size;
}

Vector2f GameView::getCenter()
{
	return mView.getCenter();
}

void GameView::reset(SceneNode* node)
{
	mWorldSize = SceneNode::mWorldSize;

	mUpperBoundary = Vector2f(mWindowSize.x / 2, mWindowSize.y / 2);
	mLowerBoundary = Vector2f(mWindowSize.x / 2, mWorldSize.y - mWindowSize.y / 2);
	mView.setSize(mWindowSize);
	mView.setViewport(FloatRect(0, -0.2, 1, 1));
	if (node)
		mView.setCenter(node->getPosition());
	else
		mView.setCenter(mLowerBoundary);
}

void GameView::update(SceneNode* node)
{
	if (node)
	{
		Vector2f position = node->getPosition();
		mView.setCenter(Vector2f(mView.getCenter().x, min(position.y, mLowerBoundary.y)));
	}
}


World::World(RenderWindow& window) : mWindow(window), mSceneGraph(), mSceneLayers()
{
	mLevel = 1;
	loadTexture();
	
	SceneNode::mTextures = &mTextures;
	SceneNode::mWindowSize = mWindow.getSize();
	
	//Create the different layers in the World
	for (int i = 0; i < LayerCount; i++)
	{
		unique_ptr<SceneNode> layer(new SceneNode());
		mSceneLayers[i] = layer.get();
		mSceneGraph.attachChild(move(layer));
	}

	/******************************
	Intitalize the background layer
	******************************/

	//The background
	unique_ptr<SpriteNode> backgroundSprite(new SpriteNode());
	mWorldPointers.mSpriteNode = backgroundSprite.get();
	mSceneLayers[BackGround]->attachChild(move(backgroundSprite));

	//The HUD
	unique_ptr<GameHUD> HUDSprite(new GameHUD());
	mWorldPointers.mHUD = HUDSprite.get();
	mSceneLayers[BackGround]->attachChild(move(HUDSprite));
	
	/********************************
	Intitalize the middleground layer
	********************************/

	unique_ptr<Platform> TileSet(new Platform());
	mWorldPointers.mPlatform = TileSet.get();
	mSceneLayers[MiddleGround]->attachChild(move(TileSet));


	/******************************
	Intitalize the foreground layer
	******************************/

	//Create the different node layers for different entities
	for (int i = 0; i < UnitCount - 2; i++)
	{
		unique_ptr<SceneNode> layer(new SceneNode());
		mSceneUnits[i] = layer.get();
		mSceneLayers[ForeGround]->attachChild(move(layer));
	}
	unique_ptr<Objects> layer(new Objects());
	mSceneUnits[1] = layer.get();
	mSceneLayers[ForeGround]->attachChild(move(layer));
	
	unique_ptr<Opponent> opponent(new Opponent());
	mSceneUnits[2] = opponent.get();
	mWorldPointers.mOpponent = opponent.get();
	//mWorldPointers.mOpponent->mWorldTrigger = [this](){ mLevel++; buildScene(mLevel); };
	mSceneLayers[ForeGround]->attachChild(move(opponent));
		
	//Build the Scene
	buildScene(mLevel);
}

void World::loadTexture()
{
	mTextures.load(Textures::Platform, "Images/Platform.png");
	mTextures.load(Textures::Tank, "Images/Tank.png");
	mTextures.load(Textures::TankExplosion, "Images/TankExplosion.png");
	mTextures.load(Textures::Platform, "Images/Platform.png");
	mTextures.load(Textures::HUDBanner, "Images/HUDBanner.png");
	mTextures.load(Textures::Wolf, "Images/Drone_Basic.png");
	mTextures.load(Textures::PowerUps, "Images/PowerUps.png");
	mTextures.load(Textures::Vanish, "Images/Vanish.png");

	SceneNode::mSFX.load(SFX::PlayerDeath, "Audio/Explosion.ogg");
	SceneNode::mSFX.load(SFX::RoboRepair, "Audio/Repair.ogg");
	SceneNode::mSFX.load(SFX::RoboEntry, "Audio/RoboEntry.ogg");
	SceneNode::mSFX.load(SFX::RoboDelete, "Audio/RoboDelete.ogg");
	SceneNode::mSFX.load(SFX::NewKey, "Audio/KeyGeneration.ogg");
	SceneNode::mSFX.load(SFX::PowerObtained, "Audio/PowerObtained.ogg");
	SceneNode::mSFX.load(SFX::PortalExplosion, "Audio/PortalExplosion.ogg");
	SceneNode::mSFX.load(SFX::RoboVanish, "Audio/RoboVanish.ogg");
}

void World::buildScene(int level)
{
	//Load the level
	mWorldPointers.mPlatform->loadLevel(level);	
	SceneNode::mWorldSize = mWorldPointers.mPlatform->getWorldSize();
	
	//Set the View
	mGameView.setWindowSize(Vector2f(mWindow.getSize().x, 0.8 * mWindow.getSize().y));
	mGameView.reset(nullptr);
	
	//Spawn the Player
	unique_ptr<Player> player(new Player());
	mWorldPointers.mPlayer = player.get();	
	mWorldPointers.mPlayer->setSpawnPosition(Vector2f(0, 400));	
	mWorldPointers.mPlayer->mWorldTrigger = [this](){ endLevel(0); };
	mSceneUnits[PlayerUnit]->attachChild(move(player));
	mWorldPointers.mHUD->setPlayerPtr(mWorldPointers.mPlayer);

	//Spawn the Enemy
	mWorldPointers.mOpponent->spawn(7);

	Time dt;
	while (!SceneNode::mCommandQueue.isEmpty())
		mSceneGraph.onCommand(SceneNode::mCommandQueue.pop(), dt);
}

void World::draw()
{
	mWindow.setView(mWindow.getDefaultView());
	mWindow.draw(*mSceneLayers[BackGround]);

	mWindow.setView(mGameView.mView);
	mWindow.draw(*mSceneLayers[MiddleGround]);
	mWindow.draw(*mSceneLayers[ForeGround]);
}

void World::update(Time dt)
{		
	mSceneGraph.update(dt);
		
	handleCollision();
		
	while (!SceneNode::mCommandQueue.isEmpty())
		mSceneGraph.onCommand(SceneNode::mCommandQueue.pop(), dt);
		
	mGameView.update(mWorldPointers.mPlayer->getPlayerNode());

	SceneNode::mSFX.update(mGameView.getCenter());
	mSceneGraph.removeNodes();	

	if (mWorldPointers.mOpponent->getSpawnerCount() <= 0)
	{
		mLevel++;
		buildScene(mLevel);
	}
}

void World::handleCollision()
{
	//Clock time;
	set<SceneNode::Pair> collisionPairs;
	vector<SceneNode*> collidingNodes;
	mSceneLayers[ForeGround]->checkDoesNodeCollide(collidingNodes);

	//time.restart();		
	for (auto itr = collidingNodes.begin(); itr != collidingNodes.end(); itr++)
	{
		mWorldPointers.mPlatform->checkNodeCollision(**itr, collisionPairs);
		for (auto jtr = itr + 1; jtr != collidingNodes.end(); jtr++)
			if (collision(**itr, **jtr))
				collisionPairs.insert(minmax(*itr, *jtr));
	}
	//cout << time.getElapsedTime().asMilliseconds() << endl;
		
	//mCollisionList.sweepAndPrune();
	

	for (auto pair : collisionPairs)
	{
		if (matchCategory(pair, Category::Animal, Category::TankA))
		{
			Animal& robot = static_cast<Animal&>(*pair.first);
			Tank& tank = static_cast<Tank&>(*pair.second);
			if (robot.getCurrentMode() == Modes::Repair)
			{
				robot.destroy();
				SceneNode::mSFX.playSFX(SFX::RoboDelete);
				mWorldPointers.mOpponent->spawn();
			}
			else if (!pair.second->isDestroyed())
			{
				//tank.destroy();
				//SceneNode::mSFX.playSFX(SFX::PlayerDeath);
			}
		}
		else if (matchCategory(pair, Category::Animal, Category::Animal))
		{
			static_cast<Animal*>(pair.first)->handleCollision(pair.second, bitset<8>());
		}
		else if (matchCategory(pair, Category::TankA, Category::PowerUp))
		{
			Tank& tank = static_cast<Tank&>(*pair.first);
			PowerUp& power = static_cast<PowerUp&>(*pair.second);
			power.handleCollision(&tank);
		}
	}
}

void World::endLevel(int n)
{
	if (n == 0)
		mDestroy();
}
CommandQueue& World::getCommandQueue()
{
	return SceneNode::mCommandQueue;
}
