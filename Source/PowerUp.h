#pragma once
#include "SceneNode.h"
#include "Entity.h"
extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}
enum PowerIds{RedKey, YellowKey, BlueKey, GreenKey};
class PowerUp : public Entity
{
	Sprite			mSprite;
	unsigned int	mID;
	IntRect			mTextureRect;
	bool			mMarkedForRemoval;
	SceneNode*		mNode;
public:
				PowerUp();
				PowerUp(unsigned int id);
				PowerUp(unsigned int id, Vector2f position);
	
	void		setID(unsigned int id);
	Vector2f	getSize();
	void		getCollisionRect(vector<FloatRect>& rectangles) const;

	void		drawCurrent(RenderTarget& target, RenderStates states) const;
	void		updateCurrent(Time dt);
	void		handleCollision(SceneNode* collider, int polyRect = 0);
	bool		isMarkedForRemoval();
	void		destroy();
};

class Objects : public SceneNode
{
public:
	Objects();
	void		createPowerUp(unsigned int ID, Vector2f position);
	void		useKey(unsigned int keyId, Vector2f lockPosition);
};