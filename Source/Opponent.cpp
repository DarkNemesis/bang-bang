#include "Opponent.h"
#include "Animal.h"

Opponent::Opponent() : mRandomDistributor(0, 100)
{
	mCategory = Category::Opponent;
	mSpawnQueue = 0;
	mSpawnDelay = 3;
	mSpawnTimer = 3;
	mKeyRequestStack = 0;
}

void Opponent::addSpawner(Spawner* spawner)
{
	mSpawners.push_back(spawner);
}

void Opponent::refreshSpawnerList()
{
	for (auto itr = mSpawners.begin(); itr != mSpawners.end(); itr++)
	{
		if ((*itr)->getStatus() < 0)
		{
			auto last = mSpawners.end() - 1;
			*itr = *last;
			mSpawners.pop_back();
		}
	}
}

int	Opponent::getSpawnerCount()
{
	return mSpawners.size();
}

void Opponent::spawn(int n)
{
	mSpawnQueue += n;
}

void Opponent::requestForKeyGeneration()
{
	mKeyRequestStack++;

	int enemyCount = mChildren.size();
	if (enemyCount >= 5)
		mKeyProb = 5 * (enemyCount + 10);
	else
		mKeyProb = enemyCount * 10;

	double rand = mRandomDistributor(mRandomGenerator);

	if (rand <= mKeyProb)
	{
		int interval = 100 / enemyCount;
		mKeyProb = interval;
		for (auto itr = mChildren.begin(); itr < mChildren.end(); itr++)
		{
			double rand2 = mRandomDistributor(mRandomGenerator);
			Animal *wolf = static_cast<Animal*>((*itr).get());
			wolf->recoverFromRepair();
			if (rand2 <= mKeyProb && !wolf->haveKey())
			{
				wolf->giveKey();
				mKeyRequestStack--;
				return;
			}
			else
				mKeyProb += interval;
		}
	}
	else
		mKeyProb = 100 / (10 - enemyCount);
}

void Opponent::updateCurrent(Time dt)
{
	if (mSpawnTimer < mSpawnDelay)
		mSpawnTimer += dt.asSeconds();
	if (mSpawnQueue != 0 && mSpawnTimer >= mSpawnDelay)
	{
		unique_ptr<Wolf> wolf(new Wolf(*mTextures));
		double rand = mRandomDistributor(mRandomGenerator);
		int prob = ceil(100.f / mSpawners.size());

		array<Vector2f, 2> spawnInfo = mSpawners[rand / prob]->getSpawnInfo();

		if (spawnInfo[1].x == -1)
			wolf->setPosition(spawnInfo[0].x - wolf->getSize().x, spawnInfo[0].y - wolf->getSize().y);
		else
			wolf->setPosition(spawnInfo[0].x, spawnInfo[0].y - wolf->getSize().y);

		wolf->setVelocity(Vector2f(100 * spawnInfo[1].x, 0));
		
		if (mKeyRequestStack)
		{
			double rand2 = mRandomDistributor(mRandomGenerator);
			if (rand2 <= mKeyProb)
			{
				wolf->giveKey();
				mKeyRequestStack--;
			}
			else
				mKeyProb += 10;
		}
		
		attachChild(std::move(wolf));
		mSpawnTimer = 0;
		mSpawnQueue--;
	}
}

void Opponent::warp(SceneNode* node)
{
	Animal *wolf = static_cast<Animal*>(node);
	double rand = mRandomDistributor(mRandomGenerator);

	int prob = ceil(100.f / mSpawners.size());
	array<Vector2f, 2> spawnInfo = mSpawners[rand / prob]->getSpawnInfo();

	if (spawnInfo[1].x == -1)
		wolf->setPosition(spawnInfo[0].x - wolf->getSize().x, spawnInfo[0].y - wolf->getSize().y);
	else
		wolf->setPosition(spawnInfo[0].x, spawnInfo[0].y - wolf->getSize().y);

	wolf->setVelocity(Vector2f(100 * spawnInfo[1].x, 0));
	/*
	if (rand <= mSpawnDirProb)
	{
		wolf->setPosition(mSpawners[0].getPosition());
		wolf->setVelocity(Vector2f(abs(wolf->getVelocity().x * 1.2), 0));
		mSpawnDirProb *= (mSpawnDirProb / 100);
	}
	else
	{
		wolf->setPosition(mSpawners[0].getPosition());
		wolf->setVelocity(Vector2f(-abs(wolf->getVelocity().x * 1.2), 0));
		mSpawnDirProb *= (2 - mSpawnDirProb / 100);
	}*/
}