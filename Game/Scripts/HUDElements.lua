_tNumber =
{
	{left = 0, 		top = 0, width = 40, height = 40},
	{left = 40, 	top = 0, width = 40, height = 40},
	{left = 80, 	top = 0, width = 40, height = 40},
	{left = 120, 	top = 0, width = 40, height = 40},
	{left = 160, 	top = 0, width = 40, height = 40},
	{left = 200, 	top = 0, width = 40, height = 40},
	{left = 240, 	top = 0, width = 40, height = 40},
	{left = 280, 	top = 0, width = 40, height = 40},
	{left = 320, 	top = 0, width = 40, height = 40},
	{left = 360, 	top = 0, width = 40, height = 40}
}

_tKey = 
{
	Yellow = 	{left = 0, 		top = 40, width = 40, height = 40},
	Green = 	{left = 40, 	top = 40, width = 40, height = 40},
	Red = 		{left = 80, 	top = 40, width = 40, height = 40},
	Blue = 		{left = 120, 	top = 40, width = 40, height = 40}
}

_tMultiplier = 
{
	left = 320, top = 40, width = 40, height = 40
}

_tHealth = 
{
	Full = 	{left = 160, 	top = 40, width = 40, height = 40},
	Half = 	{left = 200, 	top = 40, width = 40, height = 40},
	Empty = {left = 240, 	top = 40, width = 40, height = 40}
}

PlayerDetails =
{
	Life = 0,
	Health = 1,
	Keys = {Yellow = 0, Red = 0, Blue = 0, Green = 0},
	KeyValue = 0
}

function setHealth()
	if PlayerDetails.Health == 1 then
		vertex.texture(PlayerHUD, 1, _tHealth.Full)
	elseif PlayerDetails == 0 then
		vertex.texture(PlayerHUD, 1, _tHealth.Half)
	end
end

function setLife()
	d1 = math.floor(PlayerDetails.Life / 10) % 10;
	vertex.texture(PlayerHUD, 3, _tNumber[d1 + 1]);

	d2 = PlayerDetails.Life % 10;
	vertex.texture(PlayerHUD, 4, _tNumber[d2 + 1]);
end

function keyMap(keystring)
	if keystring == "Red" then return 0
	elseif keystring == "Yellow" then return 1
	elseif keystring == "Green" then return 2
	elseif keystring == "Blue" then return 3
	end
end

function setKeys()
	p = 0;
	for key,value in pairs(_tKey) do
		d1 = PlayerDetails.Keys[key] % 10;
		vertex.texture(PlayerHUD, 7 + keyMap(key) * 3, _tNumber[d1 + 1]);
		p = p + 1;
	end
end

function init(BannerSize)

	vertex.resize(PlayerHUD, 16 * 4);

	frame = {x = 1360 * BannerSize.width / 1366, y = 152 * BannerSize.height / 153.6};
	countInv = {x = 1 / 34, y = 1 / 3}
	size = {x = countInv.x * frame.x, y = countInv.y * (frame.y * 120 / 152)};
	offset = {x = size.x + BannerSize.left, y = BannerSize.top + (frame.y - (size.y / countInv.y)) / 2}
	
	--Health
	setHealth();																										--Health Texture

	_pHealth = { left = offset.x, top = offset.y, width = size.x, height = size.y};										--Health Position
	vertex.position(PlayerHUD, 1, _pHealth);

	vertex.texture(PlayerHUD, 2, _tMultiplier);																			--Health Multiplier Texture
	vertex.position(PlayerHUD, 2, { left = offset.x + 2 * size.x, top = offset.y, width = size.x, height = size.y});	--Health Multiplier Position

	--Life
	setLife();																											--Life digits Texture

	_pLife1 = { left = offset.x + 3 * size.x, top = offset.y, width = size.x, height = size.y};							--Life Digit1 Position
	vertex.position(PlayerHUD, 3, _pLife1);

	_pLife2 = { left = offset.x + 4 * size.y, top = offset.y, width = size.x, height = size.y};							--Life Digit2 Position
	vertex.position(PlayerHUD, 4, _pLife2);

	--Keys
	setKeys();

	--Red Key
	vertex.texture(PlayerHUD, 5, _tKey.Red);
	vertex.position(PlayerHUD, 5, { left = offset.x + 9 * size.x, top = offset.y, width = size.x, height = size.y});
	
	vertex.texture(PlayerHUD, 6, _tMultiplier);
	vertex.position(PlayerHUD, 6, { left = offset.x + 11 * size.x, top = offset.y, width = size.x, height = size.y});

	vertex.position(PlayerHUD, 7, { left = offset.x + 12 * size.x, top = offset.y, width = size.x, height = size.y});

	--Yellow Key
	vertex.texture(PlayerHUD, 8, _tKey.Yellow);
	vertex.position(PlayerHUD, 8, { left = offset.x + 9 * size.x, top = offset.y + 2 * size.y, width = size.x, height = size.y});
	
	vertex.texture(PlayerHUD, 9, _tMultiplier);
	vertex.position(PlayerHUD, 9, { left = offset.x + 11 * size.x, top = offset.y + 2 * size.y, width = size.x, height = size.y});

	vertex.position(PlayerHUD, 10, { left = offset.x + 12 * size.x, top = offset.y + 2 * size.y, width = size.x, height = size.y});

	--Green Key
	vertex.texture(PlayerHUD, 11, _tKey.Green);
	vertex.position(PlayerHUD, 11, { left = offset.x + 15 * size.x, top = offset.y, width = size.x, height = size.y});

	vertex.texture(PlayerHUD, 12, _tMultiplier);
	vertex.position(PlayerHUD, 12, { left = offset.x + 17 * size.x, top = offset.y, width = size.x, height = size.y});

	vertex.position(PlayerHUD, 13, { left = offset.x + 18 * size.x, top = offset.y, width = size.x, height = size.y});

	--Blue Key
	vertex.texture(PlayerHUD, 14, _tKey.Blue);
	vertex.position(PlayerHUD, 14, { left = offset.x + 15 * size.x, top = offset.y + 2 * size.y, width = size.x, height = size.y});

	vertex.texture(PlayerHUD, 15, _tMultiplier);
	vertex.position(PlayerHUD, 15, { left = offset.x + 17 * size.x, top = offset.y + 2 * size.y, width = size.x, height = size.y});

	vertex.position(PlayerHUD, 16, { left = offset.x + 18 * size.x, top = offset.y + 2 * size.y, width = size.x, height = size.y});
end

function update(life, Keys)
	
	if PlayerDetails.Life ~= life then
		PlayerDetails.Life = life;
		setLife();
	end

	keyValue = 0;
	for key,value in pairs(Keys) do
		keyValue = keyValue + (10 ^ keyMap(key)) * value;
	end
	if keyValue ~= PlayerDetails.KeyValue then
		PlayerDetails.KeyValue = keyValue;
		PlayerDetails.Keys = Keys;
		setKeys();
	end

end