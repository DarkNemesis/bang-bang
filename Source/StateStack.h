#pragma once
#include <vector>
#include <memory>
#include <functional>
#include <map>
#include <utility>
#include "Identifiers.h"
#include "State.h"
using namespace std;

enum Action
{
	Push, Pop, Clear
};
class StateStack
{
	struct PendingChange
	{
		Action mAction;
		States::States mStateID;
		explicit PendingChange(Action action, States::States stateID = States::None) : mAction(action), mStateID(stateID) {}
	};
	vector<unique_ptr<State>> mStack;
	vector<PendingChange> mPendingList;
	State::Context mContext;
	map<States::States, std::function<unique_ptr<State>()>> mFactories;

	unique_ptr<State> createState(States::States stateID);
	void applyPendingChanges();
public:
	explicit StateStack(State::Context context);
	~StateStack(void);

	void update(Time dt);
	void draw();
	void processEvent(Event& event);

	void pushState(States::States stateID);
	void popState();
	void clearState();

	bool isEmpty() const;

	template <typename T>
	void registerState(States::States stateID);
};

template <typename T>
void StateStack::registerState(States::States stateID)
{
	mFactories[stateID] = [this] ()
	{
		return unique_ptr<State>(new T(*this, mContext));
	};
}