#pragma once
#include "State.h"
#include "ParallelTask.h"
#include "Utility.h"
class LoadingState :
	public State
{
	Sprite mSprite;
	RectangleShape mProgress;
	RectangleShape mFill;
	ParallelTask mTask;
	RenderWindow& mWindow;
public:
	LoadingState(StateStack& Stack, Context context);
	~LoadingState(void);
	virtual void draw();
	virtual bool update(Time dt);
	virtual bool processEvent(Event& event);
};

