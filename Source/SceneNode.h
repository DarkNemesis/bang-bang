#pragma once
#include <SFML/Graphics.hpp>
#include <memory>
#include <bitset>
#include <set>
#include <vector>
#include "Identifiers.h"
#include "Command.h"
#include "Utility.h"
#include "SoundPlayer.h"
#include <iostream>

using namespace sf;
using namespace std;

class SceneNode : public Transformable, public Drawable, private NonCopyable
{
public :
	
	typedef unique_ptr<SceneNode> SN_PTR;
	typedef pair<SceneNode*, SceneNode*> Pair;

protected : 
	vector<unique_ptr<SceneNode>> mChildren;
	SceneNode* mParent;
	unsigned int mCategory;
	unsigned int mCollidingCategory;
	Vector2f mSize;
	bool mDoesItCollide{ 0 };
public:	
	static TextureHolder* mTextures;
	static CommandQueue mCommandQueue;
	static Vector2u mWindowSize;
	static Vector2f mWorldSize;
	static SoundPlayer mSFX;

	//Functions for packing children
	void					attachChild(unique_ptr<SceneNode> child);
	unique_ptr<SceneNode>	detachChild(SceneNode& node);
	
	//Updating functions
	void					update(Time dt);
	virtual void			updateCurrent(Time dt);
	void					updateChildren(Time dt);

	//Rendering functions
	virtual void			draw(RenderTarget& target,RenderStates states) const;
	virtual void			drawCurrent(RenderTarget& target, RenderStates states) const;
	void					drawChildren(RenderTarget& target, RenderStates states) const;

	virtual unsigned int	getCategory() const;
	virtual void			setCategory(unsigned int category);
	virtual unsigned int	getCollidingCategory() const;
	virtual void			setCollidingCategory(unsigned int category);

	virtual Vector2f		getSize();
	virtual void			setSize(Vector2f size);
	
	virtual void			getCollisionRect(vector<FloatRect>& rectangles) const;

	void					onCommand(Command& command, Time dt);
	Transform				getWorldTransform() const;	
	
	//Collision functions
	virtual void			checkNodeCollision(SceneNode& node, set<Pair>& collisionPairs);
	virtual void			checkSceneCollision(SceneNode& sceneGraph, set<Pair>& collisionPairs);
	virtual void			checkDoesNodeCollide(vector<SceneNode*>& collidingNodes);
	
	//Node removing functions
	virtual bool			isMarkedForRemoval();
	virtual bool			isDestroyed();
	void					removeNodes();
};


class SpriteNode : public SceneNode
{
	Sprite mSprite;
public:
	SpriteNode();
	virtual void drawCurrent(RenderTarget& target, RenderStates states) const;
};

