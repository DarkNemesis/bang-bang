#include "Player.h"
#include "PowerUp.h"

Player::Player()
{
	mCategory = Category::Player;
	mPlayerLife = 3;
	mSpawnDelay = 1 * 1000 * 1000;
	mSpawnClock = 0;
}

void Player::setSpawnPosition(Vector2f position)
{
	mSpawnPosition = position;
}

SceneNode* Player::getPlayerNode()
{
	if (mChildren.size() != 0)
		return mChildren.front().get();
	else return nullptr;
}

void Player::updateCurrent(Time dt)
{
	if (mChildren.size() == 0)
	{
		if (mSpawnClock > 0)
			mSpawnClock -= dt.asMicroseconds();
		else if (mPlayerLife)
		{
			unique_ptr<Tank> player(new Tank());
			mPlayerNode = player.get();
			mPlayerNode->setPosition(Vector2f(0, 400));
			attachChild(std::move(player));
			mPlayerLife--;
			mSpawnClock = mSpawnDelay;
		}
		else
			mWorldTrigger();
	}
}

void Player::addKey(unsigned int key)
{
	mKeyQuantity[key]++;
}

void Player::checkAndUseKey(Spawner* spawner)
{
	int key = spawner->getStatus();
	if (key < 0 || key > 3)
		return;
	if (mKeyQuantity[key])
	{
		mKeyQuantity[key]--;
		mSFX.playSFX(SFX::PortalExplosion);

		spawner->activateKey();

		/*Command cmd;
		cmd.mCategory = Category::Objects;
		cmd.mAction = [key, pos](SceneNode& node, Time dt)
		{Objects& obj = static_cast<Objects&>(node); obj.useKey(key, pos); };
		mCommandQueue.push(cmd);*/
	}
}

Player::~Player(void)
{}

void PlayerInput::handleRealtimeInput(CommandQueue& commands)
{
	if (Keyboard::isKeyPressed(Keyboard::Key::Left))
	{
		Command moveLeft;
		moveLeft.mCategory = Category::TankA;
		moveLeft.mAction = TankMover(-900, 0);
		commands.push(moveLeft);
	}
	if (Keyboard::isKeyPressed(Keyboard::Key::Right))
	{
		Command moveRight;
		moveRight.mCategory = Category::TankA;
		moveRight.mAction = TankMover(900, 0);
		commands.push(moveRight);
	}	

	if (Keyboard::isKeyPressed(Keyboard::Key::Up))
	{
		Command jump;
		jump.mCategory = Category::TankA;
		jump.mAction = [](SceneNode& node, Time dt) {Tank& tank = static_cast<Tank&>(node); tank.jump();  };
		commands.push(jump);
	}	
}

void PlayerInput::handleEvent(const Event& event, CommandQueue& commands)
{
}