#include "GUI.h"
#include "Utility.h"
namespace GUI
{	
	//	--- CONTAINER DEFINITIONS ---	//

	Container::Container(RenderWindow& window) : mWindow(window)
	{}

	void Container::update()
	{
		if (!Mouse::isButtonPressed(Mouse::Button::Left))
			for (auto itr = mChildren.rbegin(); itr != mChildren.rend(); itr++)
				if ((*itr) -> boundingBox().contains(Vector2f(Mouse::getPosition(mWindow))))
					(*itr) -> highlight(true);
				else
					(*itr) -> highlight(false);
	}

	void Container::handleEvent(const Event& event)
	{
		for (auto itr = mChildren.rbegin(); itr != mChildren.rend(); itr++)
		{
			if ((*itr) -> handleEvent(event))
				break;
		}

		if (event.type == Event::MouseButtonPressed)
		{
			for (auto itr = mChildren.rbegin(); itr != mChildren.rend(); itr++)
				if((*itr) -> boundingBox().contains(Vector2f(Mouse::getPosition(mWindow))))
					(*itr) -> select(true);
		}

		else if (event.type == Event::MouseButtonReleased)
			for (auto itr = mChildren.rbegin(); itr != mChildren.rend(); itr++)
			{
				if ((*itr) -> boundingBox().contains(Vector2f(Mouse::getPosition(mWindow))) && (*itr) -> isSelected())
				{
					(*itr) -> execute();
					break;
				}
				else 
					(*itr) -> select(false);				
			}
	}

	void Container::pack(Component* component)
	{
		mChildren.push_back(component);
	}

	void Container::draw(RenderTarget& target, RenderStates states) const
	{
		states.transform *= getTransform();
		for(auto itr = mChildren.rbegin(); itr != mChildren.rend(); ++itr)
			(*itr) -> draw(target, states);
	}


	//	--- COMPONENT DEFINITIONS ---	//

	Component::Component() : _isSelected(false), _isHighlighted(false), mFinish(false)
	{}

	bool Component::handleEvent(const Event& event)
	{return false;}

	void Component::highlight(bool highlight)
	{
		_isHighlighted = highlight;
	}
	
	void Component::select(bool select)
	{
		_isSelected = select;
	}
		
	bool Component::isSelected()
	{
		return _isSelected;
	}

	bool Component::isFinished()
	{
		return mFinish;
	}
	

	//	--- BUTTON DEFINITIONS ---	//

	Button::Button(eButtonType type)
	{
		if (type == Textured || type == TexLabeled)
			_isBackgroundEnabled = false;
		else
		{
			_isBackgroundEnabled = true;
			mColors[Background][Normal] = Color::Black;
			mColors[Background][Highlighted] = Color::Green;
			mColors[Background][Selected] = Color::Red;
			mColors[Background][Disabled] = Color(128, 128, 128);
		}
		
		mButtonType = type;
	
		mColors[Border][Normal]		= Color::Black;
		mColors[Border][Highlighted]= Color::Green;
		mColors[Border][Selected]	= Color::Red;
		mColors[Border][Disabled]	= Color(128, 128, 128);

		setPosition(Vector2f(20, 20));
		setOrigin(Vector2f(0, 0));
		
		mButtonShape = Rectangle;
		unique_ptr<RectangleShape> rect(new RectangleShape);
		rect.get()->setSize(Vector2f(40, 40));
		rect.get()->setOrigin(20, 20);
		rect.get()->setOutlineThickness(0);
		mButton = std::move(rect);

		mAction = [](){};
	}
	
	void Button::setAction(function<void()> execute)
	{
		mAction = std::move(execute);
	}

	void Button::setShape(eButtonShape shape)
	{		
	}

	void Button::setOutlineThickness(int thickness)
	{
		mButton->setOutlineThickness(thickness);
	}

	void Button::setSize(int x, int y)
	{
		if (mButtonShape == Rectangle)
			static_cast<RectangleShape*>(mButton.get())->setSize(Vector2f(x, y));			
		else
			static_cast<CircleShape*>(mButton.get())->setRadius(x);

		FloatRect bounds = mButton->getLocalBounds();
		mButton->setOrigin(bounds.width / 2.f, bounds.height / 2.f);
		mButton->setPosition(bounds.width / 2.f, bounds.height / 2.f);
	}

	void Button::switchState(eButtonState target)
	{
		mButton->setOutlineColor(mColors[Border][target]);
		if (mButtonType == Textured || mButtonType == TexLabeled)
			mButton->setTexture(mTexture[target]);
		if (mButtonType == Labeled || mButtonType == TexLabeled)
			mLabel.setColor(mColors[Label][target]);
		if (_isBackgroundEnabled == true)
			mButton->setFillColor(mColors[Background][target]);		
	}

	void Button::highlight(bool highlight)
	{
		_isHighlighted = highlight;

		if (highlight)
			switchState(Highlighted);
		else 
			switchState(Normal);		
	}
	
	void Button::select(bool select)
	{
		_isSelected = select;

		if(select)
			switchState(Selected);
		else
			switchState(Normal);		
	}

	bool Button::isSelected()
	{
		return _isSelected;
	}

	FloatRect Button::boundingBox()
	{
		if (mButton->getGlobalBounds().height == 0 && mButton->getGlobalBounds().width == 0)
			return getTransform().transformRect(mLabel.getGlobalBounds());
		else
			return getTransform().transformRect(mButton->getGlobalBounds());
	}

	void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();		
		if (mButtonType == Textured || mButtonType == TexLabeled)
			target.draw(*mButton, states);
		if (mButtonType == Labeled || mButtonType == TexLabeled)
			target.draw(mLabel, states);		
	}	

	void Button::execute()
	{
		mAction();
		mFinish = true;
	}

	void Button::setColors(eColor target, vector<Color> colors)
	{
		for (int i = eButtonState::Normal; i < colors.size(); i++)
			mColors[target][i] = colors[i];
		if (colors.size() != 4)
			mColors[target][Disabled] = Color(128, 128, 128);
	}

	void Button::setTexture(vector<Texture*> textures)
	{
		for (int i = eButtonState::Normal; i < textures.size(); i++)
			mTexture[i] = textures[i];
		if (textures.size() != 4)
			mTexture[Disabled] = mTexture[Normal];

		if (mButtonType == Textured || mButtonType == TexLabeled)
			mButton->setTexture(mTexture[Normal]);

		IntRect bounds = mButton->getTextureRect();
		if (mButtonShape == Rectangle)
			static_cast<RectangleShape*>(mButton.get())->setSize(Vector2f(bounds.width, bounds.height));
		else
			static_cast<CircleShape*>(mButton.get())->setRadius(min(bounds.width, bounds.height));
		
		mButton->setOrigin(bounds.width / 2.f, bounds.height / 2.f);
	}

	void Button::setLabel(string label, Font& font, unsigned int size)
	{
		mLabel.setString(label);
		mLabel.setScale(1, 1);
		mLabel.setFont(font);
		mLabel.setCharacterSize(size);
		
		FloatRect bounds = mLabel.getGlobalBounds();

		mLabel.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
		mLabel.setPosition(-bounds.left, -bounds.top);
	}

	//	--- TEXTBOX DEFINITIONS ---	//

	TextBox::TextBox(Vector2f position, Vector2f size, shared_ptr<string> destString) : mDestString(destString)
	{
		centerOrigin(mShape);		

		mShape.setSize(size);
		mText.setCharacterSize(size.y - 6);

		mShape.setPosition(position);		
		mText.setPosition(position);

		mKeyLength = 100;
	}

	bool TextBox::handleEvent(const Event& event)
	{
		if (_isSelected == true)
		{
			if (event.type == Event::TextEntered)
			{		
				if (event.text.unicode == 8)
				{
					string newString = mText.getString();
					if (newString.begin() != newString.end())
						newString.pop_back();
					mText.setString(newString);
				}
				else if (event.text.unicode == 13)
				{
					string key = mText.getString();
					(*mDestString).clear();
					(*mDestString).append(key);
					mExecute();
					return true;
				}
				else if (mText.getString().getSize() < mKeyLength)
					mText.setString(mText.getString() + event.text.unicode);
			}
		}
		return false;
	}

	void TextBox::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		target.draw(mShape, states);
		target.draw(mText, states);
	}	

	FloatRect TextBox::boundingBox()
	{
		return mShape.getGlobalBounds();
	}
	
	void TextBox::execute()
	{			
	}

	void TextBox::setSize(Vector2f size)
	{
		mShape.setSize(size);
		mText.setCharacterSize(size.y - 6);
	}

	void TextBox::setPosition(Vector2f position)
	{
		mShape.setPosition(position);
		mText.setPosition(position);
	}

	void TextBox::setColor(Color color)
	{
		mText.setColor(color);
	}

	void TextBox::setString(string label)
	{
		mText.setString(label);
	}

	void TextBox::setFont(Font& font)
	{
		mText.setFont(font);
	}

	void TextBox::setBackColor(Color colorF, Color colorO)
	{
		mShape.setOutlineThickness(2);
		mShape.setFillColor(colorF);
		mShape.setOutlineColor(colorO);
	}

	void TextBox::setKeyLength(unsigned int length)
	{
		mKeyLength = length;
	}

	void TextBox::setFunction(function<void()> funct)
	{
		mExecute = funct;
	}

	void TextBox::select(bool select)
	{
		if(select && !_isSelected)
		{
			_isSelected = true;
			mText.setString("");
		}		
		_isSelected = select;
	}
}
