#include "Animal.h"
#include "Opponent.h"
#include "PowerUp.h"
#define animate mAnimation.find(mCurrentMode) -> second

Animal::Animal()
{
	mDoesItCollide = true;
	mCollidingCategory = Category::Entity_Animal;

	mSFX.playSFX(SFX::RoboEntry, getPosition());
	mHaveKey = false;

	unique_ptr<Animation> walkAnimation(new Animation(mTextures->get(Textures::Vanish), IntRect(0, 0, 128, 128)));
	walkAnimation->setNumFrames(5);
	walkAnimation->setRepeating(false);
	walkAnimation->setDuration(seconds(0.5));
	walkAnimation->setScale(0.5, 0.5);
	mAnimation.insert(make_pair(Modes::Vanish, std::move(walkAnimation)));
}

void Animal::giveKey()
{
	mHaveKey = true;
}

bool Animal::haveKey()
{
	return mHaveKey;
}

Animal::~Animal(void)
{
}

unsigned int Animal::getCategory() const
{
	return Category::Animal;
}

void Animal::updateCurrent(Time dt)
{
	if (mCurrentMode == Modes::Vanish)
	{
		if (animate->isFinished())
			destroy();
		else
			animate->update(dt);
		return;
	}

	Entity::updateCurrent(dt);
	mVelocity.x = min(max(-500.f, mVelocity.x), 500.f);
	animate -> update(dt);
			
	if (mCurrentMode == Modes::Repair && mDeathClock.getElapsedTime() >= mDeathTimer)
		recoverFromRepair();

	SolidObject::update(rectShift(mCollisionRects[0], getWorldTransform().transformRect(mAnimation.find(mCurrentMode)->second->getGlobalBounds())));
}

void Animal::recoverFromRepair()
{
	if (mCurrentMode == Modes::Repair)
	{
		mCurrentMode = Modes::Walk;
		mVelocity.x = mHaltVelocity;
		mSFX.playSFX(SFX::RoboEntry, getPosition());
	}
}

Modes::Modes Animal::getCurrentMode()
{
	return mCurrentMode;
}

void Animal::handleCollision(SceneNode* collider, bitset<8> polyRect)
{
	if (collider->getCollidingCategory() == Category::Entity_Animal)
	{
		int padding = 2;
		Animal* node(static_cast<Animal*>(collider));
		float myVel = getVelocity().x, collVel = node->getVelocity().x;
		float myPos = getPosition().x, collPos = node->getPosition().x;
		
		float collisionPoint = (myPos + collPos + node->getSize().x) / 2;
		if (myPos > collPos)
		{
			node->setPosition(collisionPoint - node->getSize().x - padding, node->getPosition().y);
			setPosition(collisionPoint + padding, getPosition().y);
		}
		else
		{
			node->setPosition(collisionPoint + padding, node->getPosition().y);
			setPosition(collisionPoint - node->getSize().x - padding, getPosition().y);
		}			
	
		//Change the Velocities
		if (myVel < 0 && collVel < 0)
		{
			if (myPos < collPos)
				node->setVelocity(Vector2f(-collVel, node->getVelocity().y));
			else
				setVelocity(Vector2f(-getVelocity().x, getVelocity().y));
		}
		else if (myVel > 0 && collVel > 0)
		{
			if (myPos > collPos)
				node->setVelocity(Vector2f(-collVel, node->getVelocity().y));
			else
				setVelocity(Vector2f(-getVelocity().x, getVelocity().y));
		}
		else
		{
			node->setVelocity(Vector2f(-collVel, node->getVelocity().y));
			setVelocity(Vector2f(-getVelocity().x, getVelocity().y));
		}
		return;
	}
	mVelocity.y = 0;
}

Vector2f Animal::getSize()
{
	FloatRect bounds = getWorldTransform().transformRect(mAnimation.find(mCurrentMode)->second->getGlobalBounds());
	return Vector2f(bounds.width, bounds.height);
}

void Animal::goInRepairMode()
{
	if (mCurrentMode != Modes::Repair)
	{
		if (!mHaveKey)
		{
			mSFX.playSFX(SFX::RoboRepair, getPosition());
			mCurrentMode = Modes::Repair;
			mDeathClock.restart();
			mHaltVelocity = 1.2 * mVelocity.x;
			mVelocity.x = 0;
		}
		else
		{
			Vector2f dest = getPosition();
			Command power;
			power.mCategory = Category::Objects;
			power.mAction = [dest, this](SceneNode& node, Time dt){Objects& power = static_cast<Objects&>(node); power.createPowerUp(PowerIds::YellowKey, dest); };
			SceneNode::mCommandQueue.push(power);
			mCurrentMode = Modes::Vanish;
			mSFX.playSFX(SFX::RoboVanish);
			destroy();
		}
	}
}

void Animal::drawCurrent(RenderTarget& target, RenderStates states) const
{
	target.draw(*(mAnimation.find(mCurrentMode))->second, states);
}

Wolf::Wolf(TextureHolder& textures) : Animal()
{
	// Preparing texture
	textures.get(Textures::Wolf).setSmooth(true);
	mDeathTimer = seconds(15.f);
	
	// Seeding walking animation
	unique_ptr<Animation> walkAnimation(new Animation(textures.get(Textures::Wolf), IntRect(0, 0, 50, 70)));
	walkAnimation -> setNumFrames(1);
	walkAnimation -> setRepeating(true);
	walkAnimation -> setDuration(seconds(0.6));
	//walkAnimation -> setScale(mWindowSize.x / 5925.92, mWindowSize.y / 4740.74);
	mAnimation.insert(make_pair(Modes::Walk, std::move(walkAnimation)));

	// Seeding Repairing animation
	unique_ptr<Animation> dieAnimation(new Animation(textures.get(Textures::Wolf), IntRect(50, 0, 50, 70)));
	dieAnimation -> setNumFrames(1);
	dieAnimation -> setRepeating(true);
	dieAnimation -> setDuration(seconds(1));
	//dieAnimation -> setScale(mWindowSize.x / 5925.92, mWindowSize.y / 4740.74);
	mAnimation.insert(make_pair(Modes::Repair, std::move(dieAnimation)));

	// Set the current animation;
	mCurrentMode = Modes::Walk;

	setCollRect();
}


void Animal::getCollisionRect(vector<FloatRect>& rectangles) const
{
	rectangles.clear();
	FloatRect bounds = getWorldTransform().transformRect(mAnimation.find(mCurrentMode)->second->getGlobalBounds());
	for (int i = 0; i < CollisionBoxesCount; i++)
		rectangles.push_back(rectShift(mCollisionRects[i], bounds));
}

void Wolf::setCollRect()
{
	Vector2f mScale = animate->getScale();
	mCollisionRects[Complete] = FloatRect(2 * mScale.x, 0 * mScale.y, 43 * mScale.x, 70 * mScale.y);
	mCollisionRects[Upper] = FloatRect(10.8 * mScale.x, 0 * mScale.y, 30 * mScale.x, 10.7 * mScale.y);
	mCollisionRects[MiddleLeft] =	FloatRect(2 * mScale.x, 10.7 * mScale.y, 23 * mScale.x, 49.3 * mScale.y);
	mCollisionRects[MiddleRight] = FloatRect(25.4 * mScale.x, 10.7 * mScale.y, 22 * mScale.x, 49.3 * mScale.y);
	mCollisionRects[Lower] = FloatRect(11 * mScale.x, 60 * mScale.y, 27 * mScale.x, 9 * mScale.y);
}