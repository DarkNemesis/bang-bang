#include "Platform.h"

Platform::Platform() : mVertices(Quads)
{
	Block::mPlatformPointer = this;
	mTexture.loadFromFile("Images/Objects.png");
	mTexture.setSmooth(true);
}

Platform::~Platform(void)
{}


void Platform::loadLevel(int levelNo)
{
	mBlockSize.x = mWindowSize.x / 30;
	mBlockSize.y = 36 * mBlockSize.x / 64;

	//Open the lua script file
	string filename = "Levels/Level" + to_string(levelNo) + ".lua";
	lua_State* L;
	L = luaL_newstate();
	luaL_openlibs(L);
	luaL_dofile(L, filename.c_str());

	//Get the data
	lua_getfield(L, -1, "layers");

	//Get the First Layer
	lua_pushinteger(L, 1);
	lua_gettable(L, -2);

	//Get the world width
	lua_getfield(L, -1, "width");
	mBlockCount.x = lua_tointeger(L, -1);
	lua_pop(L, 1);

	//Get the world height
	lua_getfield(L, -1, "height");
	mBlockCount.y = lua_tointeger(L, -1);
	lua_pop(L, 1);

	//Get the Second Layer
	lua_pushinteger(L, 2);
	lua_gettable(L, -3);

	//Get the world data
	lua_getfield(L, -1, "data");
	lua_getfield(L, -3, "data");

	for (int i = 0; i < mBlockCount.x * mBlockCount.y; i++)
	{
		lua_pushinteger(L, i + 1);
		lua_gettable(L, -3);
		lua_pushinteger(L, i + 1);
		lua_gettable(L, -3);
		if (lua_tointeger(L, -2) != 0)
			mPowerBlocks.push_back(mBlocks.size());
		int blockType = lua_tointeger(L, -1);
		if (blockType != 0)
		{
			mBlocks.push_back(Block::createNewBlock(blockType, i / mBlockCount.x, i % (int)mBlockCount.x, mBlockKeys.size()));
			mBlockKeys.push_back(mBlocks.size() - 1);
		}
		else
		{
			mBlocks.push_back(nullptr);
		}
		lua_pop(L, 2);
	}
	lua_pop(L, 4);

	//Initialize the vertices
	mVertices.resize(mBlockKeys.size() * 4);

	for (int i : mBlockKeys)
	{
		float x = mBlocks[i]->getPosition().y * mBlockSize.x;
		float y = mBlocks[i]->getPosition().x * mBlockSize.y;

		sf::Vertex* quad = &mVertices[mBlocks[i]->getBlockNumber() * 4];
		{
			quad[0].position = sf::Vector2f(x, y);
			quad[1].position = sf::Vector2f(x + mBlockSize.x, y);
			quad[2].position = sf::Vector2f(x + mBlockSize.x, y + mBlockSize.y);
			quad[3].position = sf::Vector2f(x, y + mBlockSize.y);
		}
		mBlocks[i]->mVertexPointer = quad;
		mBlocks[i]->updateTexCords();
	}
}

void Platform::checkNodeCollision(SceneNode& node, set<Pair>& collisionPairs)
{
	vector<FloatRect> tRect;
	node.getCollisionRect(tRect);
	int ctr = 0;
	map<int, bitset<8>> collisionInfo;

	bool initial_check = false;

	vector<FloatRect> collidingTiles;

	//Find the no. of tiles colliding with the entity
	int debug = tRect[0].left / mBlockSize.x;
	int i = max(0.f, min((float)mBlockCount.x - 1, tRect[0].left / mBlockSize.x));
	int I = max(0.f, min((float)mBlockCount.x - 1, (tRect[0].left + tRect[0].width) / mBlockSize.x));

	int j = max(0.f, min((float)mBlockCount.y - 1, tRect[0].top / mBlockSize.y));
	int J = max(0.f, min((float)mBlockCount.y - 1, (tRect[0].top + tRect[0].height) / mBlockSize.y));
	for (int x = i; x <= I; x++)
	{
		for (int y = j; y <= J; y++)
		{
			int map = y * mBlockCount.x + x; //Maps 2D Array in 1D Array
			if (mBlocks[map])
			{
				if (x == 0)
					collidingTiles.push_back(FloatRect(-mBlockSize.x, y * mBlockSize.y, mBlockSize.x * 2, mBlockSize.y));
				else if (x == mBlockCount.x - 1)
					collidingTiles.push_back(FloatRect(x * mBlockSize.x, y * mBlockSize.y, mBlockSize.x * 2, mBlockSize.y));
				else
					collidingTiles.push_back(FloatRect(x * mBlockSize.x, y * mBlockSize.y, mBlockSize.x, mBlockSize.y));
			}
		}
	}

	for (int jtr = 0; jtr < collidingTiles.size(); jtr++)
	{
		for (int itr = 1; itr < tRect.size(); itr++)
		{
			if (tRect[itr].intersects(collidingTiles[jtr]))
			{
				int tempX = collidingTiles[jtr].left / mBlockSize.x;
				int tempY = collidingTiles[jtr].top / mBlockSize.y;

				if (tempX < 0)
					tempX = 0;

				auto add = collisionInfo.insert(make_pair(tempY * mBlockCount.x + tempX, 0));
				add.first->second[itr] = 1;
			}
		}
	}

#define ONLY_ONE(BOX) itr->second[BOX] && itr->second.count() == 1
#define EITHER_ONE_ONLY(BOX1, BOX2) (itr->second[BOX1] || itr->second[BOX2]) && itr->second.count() == 1
#define ONLY_THREE(BOX1, BOX2, BOX3) (itr->second[BOX1] && itr->second[BOX2] && itr->second[BOX3]) && itr->second.count() == 3

	for (auto itr = collisionInfo.begin(); itr != collisionInfo.end(); itr++)
	{
		//Create an entity from the passed node
		Entity& entity = static_cast<Entity&>(node);
		if (ONLY_ONE(Lower) || ONLY_THREE(Lower, MiddleLeft, MiddleRight))
		{
			mBlocks[itr->first]->steppedOn(&entity);
		}
		else if (ONLY_ONE(Upper) || ONLY_THREE(Upper, MiddleLeft, MiddleRight))
		{
			mBlocks[itr->first]->bouncedOff(&entity);
		}
		else if (EITHER_ONE_ONLY(MiddleLeft, MiddleRight))
		{
			mBlocks[itr->first]->collidedWith(&entity, itr->second);
		}
	}
}


int Platform::mapBlockToGrid(int blockNo)
{
	Vector2i position = mBlocks[blockNo]->getPosition();
	return position.x * mBlockCount.x + position.y;
}

#include <iostream>
void Platform::generateGreenKey()
{
	random_shuffle(mPowerBlocks.begin(), mPowerBlocks.end());
	while (!mBlocks[mPowerBlocks.back()]->issueGreenKey())
		random_shuffle(mPowerBlocks.begin(), mPowerBlocks.end());
	cout << "Generated at " << mPowerBlocks.back();
	mPowerBlocks.pop_back();
}

int Platform::convertPositionToBlockNo(Vector2f position)
{
	int x = position.x / mBlockSize.x;
	int y = position.y / mBlockSize.y;
	return y * mBlockCount.x + x;
}

Vector2f Platform::convertBlockNoToPosition(int blockNo)
{
	return mVertices[blockNo * 4].position;
}

Vector2f Platform::getWorldSize()
{
	return Vector2f(mBlockCount.x * mBlockSize.x, mBlockCount.y * mBlockSize.y);
}

void Platform::drawCurrent(RenderTarget& target, RenderStates states) const
{
	states.texture = &mTexture;
	target.draw(mVertices, states);
}

unsigned int Platform::getCategory() const
{
	return Category::Block;
}

//Update all the blocks which are bouncing
void Platform::updateCurrent(Time dt)
{
	//Update all the Bouncing blocks
	for (auto& itr : mBounceStack)
	{
		float sign = pow(-1, itr.second / 10);
		Vertex *quad = itr.first->mVertexPointer;
		quad[0].position = sf::Vector2f(quad[0].position.x, quad[0].position.y - 2 * sign);
		quad[1].position = sf::Vector2f(quad[1].position.x, quad[1].position.y - 2 * sign);
		quad[2].position = sf::Vector2f(quad[2].position.x, quad[2].position.y - 2 * sign);
		quad[3].position = sf::Vector2f(quad[3].position.x, quad[3].position.y - 2 * sign);
		itr.second += 2;
		//If another block is just above this block, then bounce it as well.
		if (itr.second == 10)
		{
			int x = quad[0].position.x / mBlockSize.x;
			int y = quad[0].position.y / mBlockSize.y;
			int map = y * mBlockCount.x + x; //Maps 2D Array in 1D Array
			if (mBlocks[map])
				bounceTile(mBlocks[map]);
			itr.first->isBouncing = false;
		}
		//If this block has completed bouncing, remove it from the list
		else if (itr.second >= 20)
		{
			mBounceStack.erase(itr.first);
			break;
		}
	}
}

//Checks and bounces a particular block
void Platform::bounceTile(Block* block)
{
	block->isBouncing = true;

	//Check if it is already been bounced or not.
	bool shift = (mBounceStack.insert(make_pair(block, 0))).second;

	//Now check does it generate any power up.
	/*if (shift && (mBlocks[blockNo]->getAttributeBit() & Block::Power))
	{
	mBlocks[blockNo]->issuePowerUpCommand(&mVertices[blockNo * 4]);
	updateTileData(mVertices[blockNo * 4].position);
	}*/
}
