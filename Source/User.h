#pragma once
#include <SFML/Graphics.hpp>
#include <fstream>
#include <string>
#include <vector>
using namespace std;
using namespace sf;
class User
{
public:
	unsigned char mLife;
	Int64 mScore;
	string mUserName;
	unsigned char mWorld;
	unsigned char mLevel;
	bool mRegister;
		
public:
	User(void);
	~User(void);
	void setName(string name);
	void setBaseValue(unsigned char life, Int64 score, unsigned char world, unsigned char level);
	void fileInput(string filename);
};

