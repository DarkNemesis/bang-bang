#pragma once
#include "ResourceHolder.h"
#include "Identifiers.h"
#include <SFML/Audio.hpp>
#include <list>
#include <cmath>
using namespace sf;

class SoundPlayer : private sf::NonCopyable
{
public:
	SoundPlayer();

	void						playSFX(SFX::SFX effect);
	void						playSFX(SFX::SFX effect, sf::Vector2f position);

	void						load(SFX::SFX effect, string filename);
	void						removeStoppedSounds();
	void						setListenerPosition(sf::Vector2f position);
	sf::Vector2f				getListenerPosition() const;
	void						update(Vector2f position = Vector2f(0,0));

private:
	SFXHolder	mSoundBuffers;
	std::list<sf::Sound>		mSounds;
};

